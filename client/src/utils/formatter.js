export function messageDateTimeFormat(datetime) {
    const before = parseInt((new Date().valueOf() - datetime) / 1000);

    if (before < 60) {
        return `${before} seconds ago`;
    } else if (before < 3600) {
        if (parseInt(before / 60) === 1) {
           return `${parseInt(before / 60)} minute ago`;
        }
        return `${parseInt(before / 60)} minutes ago`;
    } else if (before < 86400) {
        if (parseInt(before / 3600) === 1) {
            return `${parseInt(before / 3600)} hour ago`;
        }
        return `${parseInt(before / 3600)} hours ago`;
    } else if (before < 2592000) {
        if (parseInt(before / 86400) === 1) {
            return `${parseInt(before / 86400)} day ago`;
        }
        return `${parseInt(before / 86400)} days ago`;
    } else if (before < 77760000) {
        if (parseInt(before / 2592000) === 1) {
            return `${parseInt(before / 2592000)} year ago`;
        }
        return `${parseInt(before / 2592000)} years ago`;
    }
    return before;
}