const { detect } = require('detect-browser');

export function isDesktopChromeBrowser() {
    const browser = detect();
    if (browser) {
        if (!browser.name.toLowerCase().includes('chrome')) {
            return false;
        }
        return !(!browser.os.toLowerCase().includes('mac') && !browser.os.toLowerCase().includes('win'));

        // console.log(browser.name);
        // console.log(browser.version);
        // console.log(browser.os);
    }
    return false;
}

export function isDesktopBrowser() {
    const browser = detect();
    if (browser) {
        if (!browser.name.toLowerCase().includes('chrome') && !browser.name.toLowerCase().includes('safari')) {
            return false;
        }
        return !(!browser.os.toLowerCase().includes('mac') && !browser.os.toLowerCase().includes('win') && !browser.os.toLowerCase().includes('linux'));

        // console.log(browser.name);
        // console.log(browser.version);
        // console.log(browser.os);
    }
    return false;
}

export function isSupportedBrowser() {
    const browser = detect();
    if (browser) {
        return !(browser.name.toLowerCase().includes('firefox') || browser.name.toLowerCase().includes('explorer'));
    }
    return false;
}

export function isMacOSSafari() {
    const browser = detect();
    if (browser) {
        return browser.name.toLowerCase().includes('safari') && browser.os.toLowerCase().includes('mac');
    }
    return false;
}

export function isIOS() {
    const browser = detect();
    console.log(browser.name);
    console.log(browser.version);
    console.log(browser.os);
    if (browser) {
        return browser.os.toLowerCase().includes('ios');
    }
    return false;
}
