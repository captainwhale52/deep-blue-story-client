import { combineReducers } from 'redux';

import app from './reducer-app';
import message from './reducer-message';
import gallery from './reducer-gallery';


export default combineReducers({
    app,
    message,
    gallery
});
