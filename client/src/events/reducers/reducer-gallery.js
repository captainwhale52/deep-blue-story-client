// import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';
import Immutable from "seamless-immutable";
import moment from 'moment';

import gallery_1 from './../../media/gallery/gallery_1_rotate.jpg';
import gallery_1_thumbnail from './../../media/gallery/gallery_1_thumbnail.jpg';
import gallery_2 from './../../media/gallery/gallery_2_rotate.jpg';
import gallery_2_thumbnail from './../../media/gallery/gallery_2_thumbnail.jpg';
import gallery_3 from './../../media/gallery/gallery_3_rotate.jpg';
import gallery_3_thumbnail from './../../media/gallery/gallery_3_thumbnail.jpg';
import gallery_4 from './../../media/gallery/gallery_4_rotate.jpg';
import gallery_4_thumbnail from './../../media/gallery/gallery_4_thumbnail.jpg';
import gallery_5 from './../../media/gallery/gallery_5_rotate.jpg';
import gallery_5_thumbnail from './../../media/gallery/gallery_5_thumbnail.jpg';
import gallery_6 from './../../media/gallery/gallery_6_rotate.jpg';
import gallery_6_thumbnail from './../../media/gallery/gallery_6_thumbnail.jpg';
import gallery_7 from './../../media/gallery/gallery_7_rotate.jpg';
import gallery_7_thumbnail from './../../media/gallery/gallery_7_thumbnail.jpg';
import gallery_8 from './../../media/gallery/gallery_8_rotate.jpg';
import gallery_8_thumbnail from './../../media/gallery/gallery_8_thumbnail.jpg';
import gallery_9 from './../../media/gallery/gallery_9_rotate.jpg';
import gallery_9_thumbnail from './../../media/gallery/gallery_9_thumbnail.jpg';
import gallery_10 from './../../media/gallery/gallery_10_rotate.jpg';
import gallery_10_thumbnail from './../../media/gallery/gallery_10_thumbnail.jpg';
import gallery_11 from './../../media/gallery/gallery_11_rotate.jpg';
import gallery_11_thumbnail from './../../media/gallery/gallery_11_thumbnail.jpg';
import gallery_12 from './../../media/gallery/gallery_12_rotate.jpg';
import gallery_12_thumbnail from './../../media/gallery/gallery_12_thumbnail.jpg';
import gallery_13 from './../../media/gallery/gallery_13_rotate.jpg';
import gallery_13_thumbnail from './../../media/gallery/gallery_13_thumbnail.jpg';


const defaultState = Immutable({
    fetching: false,
    images: [
        {
            pk: 1,
            url: gallery_1,
            thumbnail: gallery_1_thumbnail,
            title: 'The Last Goodbye Poster',
            description: 'Image description.',
            timestamp: moment("2018-03-25", "YYYY-MM-DD")
        },{
            pk: 2,
            url: gallery_2,
            thumbnail: gallery_2_thumbnail,
            title: 'The Last Goodbye Team Photo',
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 13,
            url: gallery_13,
            thumbnail: gallery_13_thumbnail,
            title: `Schrodinger's Cat - Title`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 7,
            url: gallery_7,
            thumbnail: gallery_7_thumbnail,
            title: `Schrodinger's Cat - Cover`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 3,
            url: gallery_3,
            thumbnail: gallery_3_thumbnail,
            title: `Gilly & the cat`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 4,
            url: gallery_4,
            thumbnail: gallery_4_thumbnail,
            title: `Gilly & the cat`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 5,
            url: gallery_5,
            thumbnail: gallery_5_thumbnail,
            title: `Gilly face texturing`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 8,
            url: gallery_8,
            thumbnail: gallery_8_thumbnail,
            title: `Gilly side view`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 10,
            url: gallery_10,
            thumbnail: gallery_10_thumbnail,
            title: `Gilly's house (in progress)`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 11,
            url: gallery_11,
            thumbnail: gallery_11_thumbnail,
            title: `Gilly's house (final)`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        },{
            pk: 12,
            url: gallery_12,
            thumbnail: gallery_12_thumbnail,
            title: `Schrodinger's Cat - The Cat`,
            description: 'Image description.',
            timestamp: moment("2018-03-10", "YYYY-MM-DD")
        }
    ],
});


export default function reducer(state = defaultState, action) {
    return state;
    // switch (action.type) {
    //     case 'SEND_MESSAGE_PENDING': {
    //         const messageGroups = Immutable.asMutable(state.groups, {deep: true});
    //         messageGroups.forEach((group, index) => {
    //             if (action.payload.groupId === group.pk) {
    //                 messageGroups[index].messages.push({
    //                     username: action.payload.username,
    //                     value: action.payload.value,
    //                     timestamp: new Date().valueOf(),
    //                 });
    //             }
    //         });
    //         return state.merge({ error: null, fetching: true, groups: messageGroups });
    //     }
    //
    //     case 'SEND_MESSAGE_REJECTED': {
    //         return state.merge({ error: action.payload.error, fetching: false });
    //     }
    //
    //     case 'SEND_MESSAGE_FULFILLED': {
    //         console.log(action.payload);
    //         const messageGroups = Immutable.asMutable(state.groups, {deep: true});
    //         messageGroups.forEach((group, index) => {
    //             if (parseInt(action.payload.groupId) === group.pk) {
    //                 messageGroups[index].messages.push({
    //                     username: action.payload.username,
    //                     value: action.payload.data.message,
    //                     timestamp: new Date().valueOf(),
    //                 });
    //             }
    //         });
    //         return state.merge({ error: null, fetching: false, groups: messageGroups });
    //     }
    //     //
    //     //     console.log(action.payload);
    //     //
    //     //     const messages = Immutable.asMutable(state.messages, {deep: true});
    //     //     messages.push({
    //     //         username: state.botname,
    //     //         value: action.payload.message,
    //     //         timestamp: new Date().valueOf(),
    //     //     });
    //     //
    //     //     if (action.payload.intentName === 'DBS_AskUsername') {
    //     //         if (action.payload.dialogState === 'Fulfilled') {
    //     //             return state.merge({ error: null, fetching: false, username: action.payload.sessionAttributes['Username'], messages });
    //     //         }
    //     //     }
    //     //
    //     //     return state.merge({ error: null, fetching: false, messages });
    //     //
    //     //
    //     //
    //     //     // let username =
    //     //     //
    //     //     // if (action.payload.sessionAttributes['Username']) {
    //     //     //
    //     //     // }
    //     //
    //     //
    //     //     return state.merge({ error: null, fetching: false });
    //     //     // console.log(action.payload);
    //     //     //
    //     //     // const newMessage = {
    //     //     //     intentName: action.payload.intentName,
    //     //     //     dialogState: action.payload.dialogState,
    //     //     //     messageFormat: action.payload.messageFormat,
    //     //     //     message: action.payload.message,
    //     //     //     timestamp: new Date().valueOf()
    //     //     // };
    //     //     //
    //     //     // if (newMessage.messageFormat === 'CustomPayload') {
    //     //     //     const messageJson = JSON.parse(newMessage.message);
    //     //     //     newMessage.message = messageJson.message;
    //     //     //
    //     //     //     const username = messageJson.attributes.username ? messageJson.attributes.username : state.username;
    //     //     //     const botname = messageJson.attributes.botname ? messageJson.attributes.botname : state.botname;
    //     //     //     return state.merge({ newMessage, username, botname });
    //     //     // } else {
    //     //     //     return state.merge({ newMessage });
    //     //     // }
    //     //     // return state.merge({ fetching: false, fetched: true, error: null, user });
    //     // }
    //     // case 'CONSUME_NEW_MESSAGE': {
    //     //     return state.merge({ newMessage: null });
    //     // }
    //     default: {
    //         return state;
    //     }
    // }
}
