// import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';
import Immutable from "seamless-immutable";
import moment from 'moment';


const defaultState = Immutable({
    fetching: false,
    groups: [
        {
            pk: 1,
            name: 'Food buddy',
            members: ['Evan'],
            active: true,
            apiAddress: `${process.env.BACKEND_BASE_URL}/api/messenger/chat/evan`,
            messages: [
                {
                    username: 'Evan',
                    value: `Hey Karl?`,
                    timestamp: moment().subtract(1000, 'seconds').valueOf(),
                },{
                    username: 'Karl',
                    value: `What up?`,
                    timestamp: moment().subtract(700, 'seconds').valueOf(),
                },{
                    username: 'Evan',
                    value: `I am hungry.😔`,
                    timestamp: moment().subtract(500, 'seconds').valueOf(),
                }
            ]
        },{
            pk: 2,
            name: `Web devs`,
            members: [`Tre'Saun`],
            active: false,
            apiAddress: null,
            messages: [
                {
                    username: `Tre'Saun`,
                    value: `Hey Karl. I have a <span>web project</span> going on, and I do need your help.`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Karl`,
                    value: `Sure! explain about the project.`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Tre'Saun`,
                    value: `I am making a <span>text-based real-time strategy game</span> for various platforms, like Web, Android, and iOS.`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Tre'Saun`,
                    value: `Could you explain me what <span>technical specifications</span> that I need to know for making this happen?`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Karl`,
                    value: `Sure. First, you need to choose the <span>server-side language</span>. There are a few options: Python, Ruby, C#, Java. Unless you are using an old libraries, I definitely recommend using <span>Python</span>. It has become the most popular server-side language, and it will become more popular as most of machine learning libraries are built with Python.`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Karl`,
                    value: `If you decided to go with Python, then you need to choose the framework. There are a few criteria that you need to follow: 1) <span>popularity</span>, 2) <span>community</span>, 3) <span>documents</span>. For me, <span>Django</span> is the framework that meets all these 3 criteria. It is widely used for many production level of web applications, including Instagram, Pinterest, Bitbucket. And a number of tech start-ups use Django, so easier to get help with each other.`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Tre'Saun`,
                    value: `Got it. What about the server, or hosting? I usually have used only my PC for running a server.`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Karl`,
                    value: `Ah. Any kinds of clouding server will be a good choice, such as Digital Ocean, AWS, or Google Cloud. There are some differences, but most of them are minor. I personally use <span>AWS</span> in most cases. Btw, I gotta go, so I will explain more about AWS later.`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },{
                    username: `Tre'Saun`,
                    value: `Thx man! Talk later!`,
                    timestamp: moment().subtract(10000, 'seconds').valueOf(),
                },
            ]
        }

        // {
        //     pk: 2,
        //     members: ['Blue'],
        //     apiAddress: 'https://603kij7zu9.execute-api.us-east-1.amazonaws.com/dev/chat/',
        //     messages: [
        //         {
        //             username: 'Blue',
        //             value: `I haven't talked with you for a while.`,
        //             timestamp: moment().subtract(100000, 'seconds').valueOf(),
        //         }
        //     ]
        // }
    ],
    // components: [],
    // mode: GLOBALS.SLIDE.MODE.NONE,
});


export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'SEND_MESSAGE_PENDING': {
            const messageGroups = Immutable.asMutable(state.groups, {deep: true});
            messageGroups.forEach((group, index) => {
                if (action.payload.groupId === group.pk) {
                    messageGroups[index].messages.push({
                        username: action.payload.username,
                        value: action.payload.value,
                        timestamp: new Date().valueOf(),
                    });
                }
            });
            return state.merge({ error: null, fetching: true, groups: messageGroups });
        }

        case 'SEND_MESSAGE_REJECTED': {
            return state.merge({ error: action.payload.error, fetching: false });
        }

        case 'SEND_MESSAGE_FULFILLED': {
            console.log(action.payload);
            if (!action.payload.data.message) {
                return state.merge({ error: null, fetching: false });
            }
            const messageGroups = Immutable.asMutable(state.groups, {deep: true});
            messageGroups.forEach((group, index) => {
                if (parseInt(action.payload.groupId) === group.pk) {
                    messageGroups[index].messages.push({
                        username: action.payload.data.username,
                        value: action.payload.data.message,
                        timestamp: new Date().valueOf(),
                    });
                }
            });
            return state.merge({ error: null, fetching: false, groups: messageGroups });
        }
        //
        //     console.log(action.payload);
        //
        //     const messages = Immutable.asMutable(state.messages, {deep: true});
        //     messages.push({
        //         username: state.botname,
        //         value: action.payload.message,
        //         timestamp: new Date().valueOf(),
        //     });
        //
        //     if (action.payload.intentName === 'DBS_AskUsername') {
        //         if (action.payload.dialogState === 'Fulfilled') {
        //             return state.merge({ error: null, fetching: false, username: action.payload.sessionAttributes['Username'], messages });
        //         }
        //     }
        //
        //     return state.merge({ error: null, fetching: false, messages });
        //
        //
        //
        //     // let username =
        //     //
        //     // if (action.payload.sessionAttributes['Username']) {
        //     //
        //     // }
        //
        //
        //     return state.merge({ error: null, fetching: false });
        //     // console.log(action.payload);
        //     //
        //     // const newMessage = {
        //     //     intentName: action.payload.intentName,
        //     //     dialogState: action.payload.dialogState,
        //     //     messageFormat: action.payload.messageFormat,
        //     //     message: action.payload.message,
        //     //     timestamp: new Date().valueOf()
        //     // };
        //     //
        //     // if (newMessage.messageFormat === 'CustomPayload') {
        //     //     const messageJson = JSON.parse(newMessage.message);
        //     //     newMessage.message = messageJson.message;
        //     //
        //     //     const username = messageJson.attributes.username ? messageJson.attributes.username : state.username;
        //     //     const botname = messageJson.attributes.botname ? messageJson.attributes.botname : state.botname;
        //     //     return state.merge({ newMessage, username, botname });
        //     // } else {
        //     //     return state.merge({ newMessage });
        //     // }
        //     // return state.merge({ fetching: false, fetched: true, error: null, user });
        // }
        // case 'CONSUME_NEW_MESSAGE': {
        //     return state.merge({ newMessage: null });
        // }
        default: {
            return state;
        }
    }
}
