// import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';
import Immutable from "seamless-immutable";

const defaultState = Immutable({
    // author: GLOBALS.APP.AUTHOR,
    // isTyping: false,
    passCode: '4164',
    // unlocked: !(process.env.NODE_ENV === 'production'),
    unlocked: true,
    fetching: false,
    username: 'Karl',
    botname: 'Gaff',
    messages: [{
        username: 'Gaff',
        value: `Hello?`,
        timestamp: new Date().valueOf(),
    }],
    clues: [
        // {
        //     pk: 4,
        //     type: 'clue',
        //     position: {
        //         x: -363,
        //         y: 68
        //     },
        //     angle: -7.5,
        //     message: `<br/>Could you help me in this weekend?<br/>I have an issue with the Django Rest Framework.<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Slava`,
        //     done: false,
        // },
        // {
        //     pk: 3,
        //     type: 'clue',
        //     position: {
        //         x: 281,
        //         y: 66
        //     },
        //     angle: 7,
        //     message: `Could you create more levels for <span>Escape Goat 2</span>?<br/><br/>I would like to share your GBA version of my game within my community!<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@MagicalTimeBean.`,
        //     done: false,
        // },
        // {
        //     pk: 2,
        //     type: 'clue',
        //     position: {
        //         x: -300,
        //         y: 516
        //     },
        //     angle: 5,
        //     message: `<br/><br/>Your film is so sad...<br/><br/>Also make sure to check my message on Instagram!<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>#loveinvancouver</span>`,
        //     done: false,
        // },
        // {
        //     pk: 1,
        //     type: 'clue',
        //     position: {
        //         x: -326,
        //         y: 240
        //     },
        //     angle: -15,
        //     message: `Karl,<br/><br/><span>Evan</span> texted me that you haven't answered to him, and wondered what to eat for dinner.<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Daniel`,
        //     done: false,
        // },

        {
            pk: 2,
            type: 'clue',
            position: {
                x: -409,
                y: 83
            },
            angle: -4,
            message: `I also took some notes for you while you were gone.<br/><br/>`,
            tasks: [
                {
                    pk: 1,
                    text: `<span>Evan</span> texted me that you haven't answered to him, and he was wondering what to eat for dinner.`,
                    done: false,
                },{
                    pk: 4,
                    text: `I am sorry that you didn't win the AWS hackathon; however, I think your <span>chatbot</span> is still amazing!`,
                    done: false,
                },{
                    pk: 2,
                    text: `@MagicalTimeBean wanted you to create more levels of the GBA version of <span>Escape Goat 2</span> that you made.`,
                    done: false,
                },{
                    pk: 3,
                    text: `Daniel loves your movie, <span>The Last Goodbye</span>, and he said he left a message on Instagram.`,
                    done: false,
                },{
                    pk: 5,
                    text: `Tre was asking me if you have finished the story, <span>Schrodinger's Cat</span>. He is really curious about the ending.`,
                    done: false,
                }
            ]
        }
    ],
    detail: false,
    // components: [],
    // mode: GLOBALS.SLIDE.MODE.NONE,
});


export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'SET_PHONE_UNLOCK': {
            return state.merge({ unlocked: true });
        }
        case 'BRING_CLUE_FRONT': {
            let tempClue;
            const clues = Immutable.asMutable(state.clues, {deep: true}).filter((clue, index) => {
                if (action.payload.index === clue.pk) {
                    tempClue = clue;
                }
                return action.payload.index !== clue.pk;
            });
            clues.push(tempClue);
            return state.merge({ clues: clues });
        }
        case 'ADD_NEW_CLUE': {
            const clues = Immutable.asMutable(state.clues, {deep: true});
            let isRegistered = false;
            clues.forEach((clue, index) => {
                if (action.payload.clue.pk === clue.pk) {
                    isRegistered = true;
                }
            });

            if (!isRegistered) {
                clues.push(action.payload.clue);
            }
            return state.merge({ clues: clues });
        }
        case 'MARK_CLUE_DONE': {
            let tempClue;
            const clues = Immutable.asMutable(state.clues, {deep: true}).filter((clue, index) => {
                if (action.payload.index === clue.pk) {
                    tempClue = clue;
                    tempClue.done = true;
                }
                return action.payload.index !== clue.pk;
            });
            clues.push(tempClue);
            return state.merge({ clues: clues });
        }
        case 'MARK_TASK_DONE': {
            const clues = Immutable.asMutable(state.clues, {deep: true});
            if (clues.length && clues[0].tasks) {
                clues[0].tasks.forEach((task, index) => {
                    if (action.payload.index === task.pk) {
                        task.done = true;
                    }
                });
            }
            return state.merge({ clues: clues });
        }
        case 'SHOW_DETAIL': {
            return state.merge({ detail: true });
        }
        case 'HIDE_DETAIL': {
            return state.merge({ detail: false });
        }
        case 'HIDE_CLUE': {
            const clues = Immutable.asMutable(state.clues, {deep: true});
            clues.forEach((clue, index) => {
                clue.deactivated = true;
            });
            return state.merge({ clues: clues });
        }
        case 'REMOVE_CLUE': {
            const clues = Immutable.asMutable(state.clues, {deep: true}).filter(clue => {
                let isFound = false;
                action.payload.indeces.forEach(index => {
                    if (index === clue.pk) {
                        isFound = true;
                    }
                });
                return !isFound;
            });

            return state.merge({ clues: clues });
        }
        default: {
            return state;
        }
        // case 'SET_IS_TYPING': {
        //     return state.merge({ isTyping: action.payload.isTyping });
        // }
        // case 'SET_USERNAME': {
        //     return state.merge({ username: action.payload.username });
        // }
        // case 'SEND_MESSAGE_PENDING': {
        //     const messages = Immutable.asMutable(state.messages, {deep: true});
        //     messages.push({
        //         username: action.payload.username,
        //         value: action.payload.value,
        //         timestamp: new Date().valueOf(),
        //     });
        //     return state.merge({ error: null, fetching: true, messages: messages });
        // }
        // case 'SEND_MESSAGE_REJECTED': {
        //     return state.merge({ error: action.payload.error, fetching: false });
        // }
        // case 'SEND_MESSAGE_FULFILLED': {
        //
        //     console.log(action.payload);
        //
        //     const messages = Immutable.asMutable(state.messages, {deep: true});
        //     messages.push({
        //         username: state.botname,
        //         value: action.payload.message,
        //         timestamp: new Date().valueOf(),
        //     });
        //
        //     if (action.payload.intentName === 'DBS_AskUsername') {
        //         if (action.payload.dialogState === 'Fulfilled') {
        //             return state.merge({ error: null, fetching: false, username: action.payload.sessionAttributes['Username'], messages });
        //         }
        //     }
        //
        //     return state.merge({ error: null, fetching: false, messages });
        //
        //
        //
        //     // let username =
        //     //
        //     // if (action.payload.sessionAttributes['Username']) {
        //     //
        //     // }
        //
        //
        //     return state.merge({ error: null, fetching: false });
        //     // console.log(action.payload);
        //     //
        //     // const newMessage = {
        //     //     intentName: action.payload.intentName,
        //     //     dialogState: action.payload.dialogState,
        //     //     messageFormat: action.payload.messageFormat,
        //     //     message: action.payload.message,
        //     //     timestamp: new Date().valueOf()
        //     // };
        //     //
        //     // if (newMessage.messageFormat === 'CustomPayload') {
        //     //     const messageJson = JSON.parse(newMessage.message);
        //     //     newMessage.message = messageJson.message;
        //     //
        //     //     const username = messageJson.attributes.username ? messageJson.attributes.username : state.username;
        //     //     const botname = messageJson.attributes.botname ? messageJson.attributes.botname : state.botname;
        //     //     return state.merge({ newMessage, username, botname });
        //     // } else {
        //     //     return state.merge({ newMessage });
        //     // }
        //     // return state.merge({ fetching: false, fetched: true, error: null, user });
        // }
        // case 'CONSUME_NEW_MESSAGE': {
        //     return state.merge({ newMessage: null });
        // }

    }
}
