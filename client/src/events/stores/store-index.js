import { createStore, applyMiddleware } from 'redux';
// eslint-disable-next-line no-unused-vars
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
// import promise from 'redux-promise-middleware';
import reducers from '../reducers/reducer-index';

// const middleware = applyMiddleware(thunk, createLogger()); // Enable this if you want to see Flux logs.
const middleware = applyMiddleware(thunk); // Enable this if you want to see Flux logs.
// const middleware = applyMiddleware(promise(), thunk); // Disable this if you want to see Flux logs.
export default createStore(reducers, middleware);
