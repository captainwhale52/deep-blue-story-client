import api from './api';

import GLOBALS from './../../globals/global-index';


export const sendMessage = (groupId, message, apiAddress) => (dispatch) => {
    if (!groupId || !message || !apiAddress) {
        return Promise.resolve(
        ).then(success => success);
    }

    dispatch({
        type: 'SEND_MESSAGE_PENDING',
        payload: {
            groupId: groupId,
            username: 'Karl',
            value: message
        },
    });

    const config = {};
    // const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };
    // const data = new FormData();
    // data.append('image', image);

    return api.post(apiAddress, {
        message: message
    }, config).then((success) => {
        setTimeout(() => {
            dispatch({
                type: 'SEND_MESSAGE_FULFILLED',
                payload: {
                    groupId: groupId,
                    data: success.data
                }
            });
        }, 2500);

    }).catch((error) => {
        dispatch({
            type: 'SEND_MESSAGE_REJECTED',
            payload: error,
        });
    });
};