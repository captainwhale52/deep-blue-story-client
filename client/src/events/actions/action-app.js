import api from './api';

import GLOBALS from './../../globals/global-index';

export const unlock = () => (dispatch) => {
    dispatch({
        type: 'SET_PHONE_UNLOCK',
        payload: {

        }
    });
    return Promise.resolve(
    ).then(success => success);
};

export const bringClueFront = (index) => (dispatch) => {
    dispatch({
        type: 'BRING_CLUE_FRONT',
        payload: {
            index: index
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

export const addNewClue = (clue) => (dispatch) => {
    dispatch({
        type: 'ADD_NEW_CLUE',
        payload: {
            clue: clue
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

export const removeClue = (indeces) => (dispatch) => {
    dispatch({
        type: 'HIDE_CLUE',
        payload: {
            indeces: indeces
        }
    });

    setTimeout(() => {
        dispatch({
            type: 'REMOVE_CLUE',
            payload: {
                indeces: indeces
            }
        });
    }, 750);

    return Promise.resolve(
    ).then(success => success);
};

// export const markClueDone = (index) => (dispatch) => {
//     dispatch({
//         type: 'MARK_CLUE_DONE',
//         payload: {
//             index: index
//         }
//     });
//     return Promise.resolve(
//     ).then(success => success);
// };

export const markTaskDone = (index) => (dispatch) => {
    dispatch({
        type: 'MARK_TASK_DONE',
        payload: {
            index: index
        }
    });
    return Promise.resolve(
    ).then(success => success);
};

export const showDetail = () => (dispatch) => {
    dispatch({
        type: 'SHOW_DETAIL',
        payload: {

        }
    });
    return Promise.resolve(
    ).then(success => success);
};

export const hideDetail = () => (dispatch) => {
    dispatch({
        type: 'HIDE_DETAIL',
        payload: {

        }
    });
    return Promise.resolve(
    ).then(success => success);
};

//
// export const setIsTyping = isTyping => (dispatch) => {
//     dispatch({
//         type: 'SET_IS_TYPING',
//         payload: {
//             isTyping
//         }
//     });
//     return Promise.resolve(
//     ).then(success => success);
// };
//
//
// export const setUsername = username => (dispatch) => {
//     dispatch({
//         type: 'SET_USERNAME',
//         payload: {
//             username
//         }
//     });
//     return Promise.resolve(
//     ).then(success => success);
// };




// export const consumeNewMessage = (message) => (dispatch) => {
//     dispatch({
//         type: 'CONSUME_NEW_MESSAGE',
//         payload: {
//             message
//         }
//     });
//     return Promise.resolve(
//     ).then(success => success);
// };


// return Promise.resolve(
// ).then(success => {
//     setTimeout(() => {
//         dispatch({
//             type: 'SEND_MESSAGE_FULFILLED',
//             payload: {
//                 groupId: groupId,
//                 username: 'Eason',
//                 value: 'Test message sentence.'
//             }
//         });
//     }, 2500);
// });