import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import {isDesktopChromeBrowser} from "../../utils/support";
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';
import { addNewClue, removeClue, hideDetail } from './../../events/actions/action-app';
import { isMacOSSafari } from '../../utils/support';

import gallery_5_small from './../../media/gallery/gallery_5_small.jpg';
import gallery_6_small from './../../media/gallery/gallery_6_small.jpg';
import gallery_4_small from './../../media/gallery/gallery_4_small.jpg';
import gallery_12_small from './../../media/gallery/gallery_12_small.jpg';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: `
<h1>Gilly's Story <span>(2015)</span></h1>
<p>
An animated trailer developed and rendered in <strong>Blender</strong>, a 3D open-source computer graphic software, to introduce an interactive story, Schrodinger's Cat.
</p>

<h1>Dev Team:</h1>
<p>
<strong>Modeling (Gilly, cat, house, props) modeling:</strong> Karl Kim<br/>
<strong>Texturing (Gilly, cat, house, props):</strong> Karl Kim<br/>
<strong>Rigging & Animating (Gilly, cat):</strong> Karl Kim<br/>
<strong>SFX (particle effects, physics animation):</strong> Karl Kim<br/>
<strong>Sound:</strong> Karl Kim<br/>
</p>
`
        };
    }

    toggleFullScreenState = (event) => {
        this.setState({
            fullScreen : !this.state.fullScreen,
        });
        if (this.state.fullScreen) {
            document.querySelector('#app').classList.add(this.context.styles['lock']);
        } else {
            document.querySelector('#app').classList.remove(this.context.styles['lock']);
        }
    };

    componentDidMount() {
        document.addEventListener("webkitfullscreenchange", this.toggleFullScreenState);

        document.querySelector('#app').className = this.context.styles['horizontal'];
        setTimeout(() => {
            this.props.addNewClue({
                pk: 'gilly-0',
                type: 'note',
                position: {
                    x: 550,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message
            });

            this.props.addNewClue({
                pk: 'gilly-1',
                type: 'photo',
                position: {
                    x: 644,
                    y: 580
                },
                angle: -10,
                url: gallery_5_small,
                caption: 'Gilly face texturing',
            });

            this.props.addNewClue({
                pk: 'gilly-2',
                type: 'photo',
                position: {
                    x: 20,
                    y: -20
                },
                angle: 5,
                url: gallery_6_small,
                caption: `Gilly's house`,
            });

            this.props.addNewClue({
                pk: 'gilly-3',
                type: 'photo',
                position: {
                    x: 194,
                    y: 670
                },
                angle: 5,
                url: gallery_4_small,
                caption: 'Gilly & the cat',
            });

            this.props.addNewClue({
                pk: 'gilly-4',
                type: 'photo',
                position: {
                    x: -388,
                    y: 632
                },
                angle: 10,
                url: gallery_12_small,
                caption: 'The black cat',
            });
        }, 500);

    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
        this.props.removeClue(['gilly-0', 'gilly-1', 'gilly-2', 'gilly-3', 'gilly-4']);
        this.props.hideDetail();

        document.removeEventListener("webkitfullscreenchange", this.toggleFullScreenState);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <iframe className={this.state.fullScreen ? null : this.context.styles['rotate']}
                        width="644" height="404"
                        src="https://www.youtube.com/embed/PEUdvF-hCHU?rel=0" frameBorder="0"
                        allow="autoplay; encrypted-media" allowFullScreen></iframe>
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addNewClue, removeClue, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
