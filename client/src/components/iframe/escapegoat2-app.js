import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';
import { bringClueFront, markTaskDone } from "./../../events/actions/action-app";
import { addNewClue, removeClue, hideDetail } from './../../events/actions/action-app';
import { isDesktopBrowser, isMacOSSafari } from '../../utils/support';

import gallery_17_small from './../../media/gallery/gallery_17.jpg';
import gallery_18_small from './../../media/gallery/gallery_18.jpg';
import gallery_19_small from './../../media/gallery/gallery_19.jpg';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.iframe = React.createRef();
        this.state = {
            fullScreen: false,
            message: `
<h1>Escape Goat 2 <span>(2013)</span></h1>
<p>
<span>* Click the game screen if it doesn't respond.</span><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Controls:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <strong>Arrow</strong>: Move<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <strong>A</strong>: Start / Jump<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <strong>S</strong>: Ram<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <strong>Enter</strong>: Pause<br/>
</p>
<p>A 2D puzzle platform game for Game Boy Advance (GBA) was designed and developed by Karl Kim. The art style of the game is inspired by a puzzle platform game released in Steam, <a href="https://store.steampowered.com/app/255340/Escape_Goat_2/" target="_blank">Escape Goat 2</a>, made by @MagicalTimeBean.</p>
<p>Under his permission, Karl created a GBA version of the Steam game using <strong>C language</strong> without using any external library, thanks to Coranac, who created a document for GBA game development, called <a href="https://www.coranac.com/tonc/text/" target="_blank">Tonc</a>.</p>
<p>Karl created his own <strong>2D physics engine</strong> by utilizing a collision detection technique, called Raycast, as well as a <strong>map editor</strong> to provide game designers a way to create custom levels.</p>
<p>The game is compiled as .gba format, which can be played on a Game Boy Advance device, or Mac / Windows via GBA emulators.
</p>`
        };
    }

    toggleFullScreenState = (event) => {
        this.setState({
            fullScreen : !this.state.fullScreen,
        });
        if (this.state.fullScreen) {
            document.querySelector('#app').classList.add(this.context.styles['lock']);
        } else {
            document.querySelector('#app').classList.remove(this.context.styles['lock']);
        }
    };

    componentDidMount() {
        if (isMacOSSafari()) {
            document.addEventListener("webkitfullscreenchange", this.toggleFullScreenState);
        }

        document.querySelector('#app').className = this.context.styles['horizontal'];
        this.props.bringClueFront(2);
        this.props.markTaskDone(2);
        setTimeout(() => {
            this.props.addNewClue({
                pk: 'escapegoat2-0',
                type: 'note',
                position: {
                    x: 550,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message,
            });

            this.props.addNewClue({
                pk: 'escapegoat2-1',
                type: 'photo',
                position: {
                    x: 140,
                    y: 5
                },
                angle: -10,
                url: gallery_17_small,
                caption: 'Level 1',
            });

            this.props.addNewClue({
                pk: 'escapegoat2-2',
                type: 'photo',
                position: {
                    x: 330,
                    y: 500
                },
                angle: 5,
                url: gallery_18_small,
                caption: 'Map editor sprite guide',
            });

            this.props.addNewClue({
                pk: 'escapegoat2-3',
                type: 'photo',
                position: {
                    x: -144,
                    y: 648
                },
                angle: -7,
                url: gallery_19_small,
                caption: 'Tutorial level',
            });
        }, 250);


        if (isDesktopBrowser() && !isMacOSSafari()) {
           this.iframe.current.childNodes[1].contentWindow.focus()
        }
    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
        this.props.removeClue(['escapegoat2-0', 'escapegoat2-1', 'escapegoat2-2', 'escapegoat2-3']);
        this.props.hideDetail();

        if (isMacOSSafari()) {
            document.removeEventListener("webkitfullscreenchange", this.toggleFullScreenState);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        } else if (isDesktopBrowser() && !isMacOSSafari()) {
            return (
                <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                    <Header />
                    <iframe src={`${process.env.PHPS_BASE_URL}/escape-goat-2/`} />
                    <Footer history={this.props.history} />
                </div>
            );
        } else {
            return (
                <div className={this.context.styles['iframe-app']}>
                    <Header />
                    <iframe id="iframe-focus" className={this.state.fullScreen ? null : this.context.styles['rotate']}
                            width="644" height="404"
                            src="https://www.youtube.com/embed/LKe6YCjnj5E?rel=0" frameBorder="0"
                            allow="autoplay; encrypted-media" allowFullScreen></iframe>
                    <Footer history={this.props.history} />
                </div>
            );
        }
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addNewClue, removeClue, bringClueFront, markTaskDone, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
