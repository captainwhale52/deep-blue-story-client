import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from './message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';

import { isDesktopChromeBrowser } from '../../utils/support';
import { bringClueFront } from "./../../events/actions/action-app";
import { addNewClue, removeClue, hideDetail } from './../../events/actions/action-app';
import gallery_31_rotate from './../../media/gallery/gallery_31_rotate.jpg';
import gallery_32_small from './../../media/gallery/gallery_32_small.jpg';
import gallery_33_small from './../../media/gallery/gallery_33.jpg';
import gallery_34_small from './../../media/gallery/gallery_34.jpg';

import smart_city_sensor_kit_guide from './../../media/smart-city-sensor-kit-guide.pdf';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: `
<h1>Atlanta Smart City <span>(2016)</span></h1>
<p>
The Public Design Workshop team at Georgia Institute of Technology have conducted smart city workshops at Atlanta in 2016, to gather public opinions about the smart city. For a part of hands-on activities of the workshop, I’ve built a <strong>standalone Arduino-based sensor box</strong> that participants can play with real-time data to understand the idea of smart cities.
</p>

<h1>Deliverable:</h1>
<p>
<a href=${smart_city_sensor_kit_guide} target="_blank">Download Smart City Sensor Kit Guide</a>
</p>

<h1>Features:</h1>
<p>
<li>An Arduino board and connect various type of sensors to measure environmental factors, including noise, temperature, humidity, altitude, Carbon Monoxide, dust level, and brightness.</li>
<li>A battery pack so that the box can work as a standalone platform for measuring environmental factors outside.</li>
<li>A real-time clock module on the board with a coin cell battery to keep track of time even though the box is turned off.</li>
<li>A SD / MMC card slot to store these measured values into an MicroSD card with timestamps.</li>
<li>A TFT screen to display measured values in real-time, and show various statuses of the box.</li>
</p>

<h1>Dev Team:</h1>
<p>
<strong>Carl DiSalvo</strong> (Project Manager, Associate professor, School of Literature, Media, and Communication, Georgia Institute of Technology)<br/>
<strong>Karl Kim</strong> (Softeware Developer, IoT integrator)<br/>
<strong>Natalie Larkins</strong> (Prototype Designer)<br/>
</p>
`
        };
    }

    componentDidMount() {
        document.querySelector('#app').className = this.context.styles['horizontal'];

        setTimeout(() => {
            this.props.addNewClue({
                pk: 'smartcity-0',
                type: 'note',
                position: {
                    x: 550,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message
            });

            this.props.addNewClue({
                pk: 'smartcity-1',
                type: 'photo',
                position: {
                    x: 32,
                    y: -45
                },
                angle: 7,
                url: gallery_32_small,
                caption: 'Sensor kit inside',
            });

            this.props.addNewClue({
                pk: 'smartcity-2',
                type: 'photo',
                position: {
                    x: 190,
                    y: 605
                },
                angle: -10,
                url: gallery_33_small,
                caption: 'Noise data graph at 85 5th St NW, Atlanta, GA 30308, USA',
            });

            this.props.addNewClue({
                pk: 'smartcity-3',
                type: 'photo',
                position: {
                    x: -380,
                    y: -20
                },
                angle: -5,
                url: gallery_34_small,
                caption: 'Sensor kit inside (top down)',
            });
        }, 500);
    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
        this.props.removeClue(['smartcity-0', 'smartcity-1', 'smartcity-2', 'smartcity-3']);
        this.props.hideDetail();
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['rotate']} >
                    <img src={gallery_31_rotate} />
                </div>
                <Footer history={this.props.history} />
            </div>
        );

        // if (process.env['NODE_ENV'] === 'production' && isDesktopChromeBrowser()) {
        //     return (
        //         <div className={this.context.styles['iframe-app']}>
        //             <Header />
        //             <iframe className={this.context.styles['rotate']} src={`${process.env.PHPS_BASE_URL}/the-last-good-bye/`} width="644px" height="404px" />
        //             <Footer history={this.props.history} />
        //         </div>
        //     );
        // } else {
        //
        // }

    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        bringClueFront, addNewClue, removeClue, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
