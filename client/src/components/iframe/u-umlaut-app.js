import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';
import { addNewClue, removeClue, hideDetail } from './../../events/actions/action-app';

import gallery_35_small from './../../media/gallery/gallery_35.jpg';
import gallery_36_small from './../../media/gallery/gallery_36.jpg';



class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            message: `
<h1>Ü (U-umlaut) <span>(2017)</span></h1>
<p>
Ü is a <span>mobile application</span> that I and my friends designed in the first semester of the Master of Digital Media program at the Centre for Digital Media, for couples who are in long-distance relationship to create their memory gallery by recording their treasure moments with Ü because when they are apart, it is difficult for them to experience life together. Our idea is to be as a platform for them to make them feel connected with each other even when they are apart from each other due to various reasons such as studying abroad, going on a business trip and etc. Through the app, Ü, they are going to <span>create their own personalized storybooks, which they are able to share the missing moments together and provide a feeling of presence and connection</span>.
</p>
<h1>Dev Team:</h1>
<p>
<strong>Project Manager:</strong> Jiayi (Chloe) Lu<br/>
<strong>Developer:</strong> Karl Kim<br/>
<strong>UX Designer:</strong> Rongna (Celine) Li<br/>
<strong>UI / Visual Designer:</strong> Yuchen (Eason) Pan<br/>
<strong>2D Artist:</strong> Hangyu (Hank) Zhang<br/>
<strong>2D Artist:</strong> Jingya Shen<br/>
<strong>2D Artist:</strong> Fei Wang
</p>
`
        };
    }

    toggleFullScreenState = (event) => {
        this.setState({
            fullScreen : !this.state.fullScreen,
        });
        if (this.state.fullScreen) {
            document.querySelector('#app').classList.add(this.context.styles['lock']);
        } else {
            document.querySelector('#app').classList.remove(this.context.styles['lock']);
        }
    };

    componentDidMount() {
        document.addEventListener("webkitfullscreenchange", this.toggleFullScreenState);

        setTimeout(() => {
            this.props.addNewClue({
                pk: 'umlaut-0',
                type: 'note',
                position: {
                    x: 500,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message
            });



            this.props.addNewClue({
                pk: 'umlaut-2',
                type: 'photo',
                position: {
                    x: -475,
                    y: 43
                },
                angle: -5,
                url: gallery_35_small,
                caption: 'Outcome story 1'
            });

            this.props.addNewClue({
                pk: 'umlaut-1',
                type: 'photo',
                position: {
                    x: -351,
                    y: 226
                },
                angle: 3,
                url: gallery_36_small,
                caption: 'Outcome story 2'
            });

        }, 1000);
    }

    componentWillUnmount() {
        this.props.removeClue(['umlaut-0', 'umlaut-1', 'umlaut-2']);
        this.props.hideDetail();
        document.removeEventListener("webkitfullscreenchange", this.toggleFullScreenState);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <iframe
                        width="644" height="404"
                        src="https://www.youtube.com/embed/EF7quL9wABw?rel=0" frameBorder="0"
                        allow="autoplay; encrypted-media" allowFullScreen></iframe>
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addNewClue, removeClue, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
