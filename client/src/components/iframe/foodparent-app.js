import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';
import { addNewClue, removeClue, hideDetail } from './../../events/actions/action-app';

import gallery_27_small from './../../media/gallery/gallery_27_small.jpg';
import gallery_20_small from './../../media/gallery/gallery_20_small.jpg';



class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: `
<h1>FoodParent <span>(2016)</span></h1>
<p>
Food Parent is a web-based application developed a part of <a href="https://www.concrete-jungle.org/" target="_blank">Concrete Jungle</a>’s Food Parent project. The application provides tools for volunteers of Concrete Jungle to register newly discovered trees on the map, virtually adopt trees, and monitor fruit ripening level of the trees using mobile devices to help Concrete Jungle to harvest more fruits from untended urban trees and donate them to the needy.
</p>
<p>
<h1>Features:</h1>
<li>Geographic Information System (GIS) mapping platform using Leaflet.</li>
<li>Custom Canvas-based marker library to render a large number of Leaflet markers without performance compromise.</li>
<li>Single Page Application (SPA) using React framework and Flux architecture (Alt.js).</li>
<li>server-side APIs using PHP and MySQL.</li>
<li>Automated e-mail notification system using Mailgun.</li>
<li>Debugging and bundling configurations using Webpack.</li>
</p>

<h1>Dev Team:</h1>
<p>
<strong>Client:</strong> Craig Durkin (Concrete Jungle)<br/>
<strong>Design & Dev:</strong> Karl Kim
</p>
`
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.addNewClue({
                pk: 'foodparent-0',
                type: 'note',
                position: {
                    x: 500,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message
            });



            this.props.addNewClue({
                pk: 'foodparent-2',
                type: 'photo',
                position: {
                    x: -412,
                    y: 100
                },
                angle: -5,
                url: gallery_20_small,
                caption: 'Food map page (desktop view)'
            });

            this.props.addNewClue({
                pk: 'foodparent-1',
                type: 'photo',
                position: {
                    x: -390,
                    y: 375
                },
                angle: 3,
                url: gallery_27_small,
                caption: 'FoodParent at Museum of Design Atlanta (MODA)'
            });

        }, 1000);
    }

    componentWillUnmount() {
        this.props.removeClue(['foodparent-0', 'foodparent-1', 'foodparent-2']);
        this.props.hideDetail();
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <iframe src={`https://www.concrete-jungle.org/food-map/`} />
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addNewClue, removeClue, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
