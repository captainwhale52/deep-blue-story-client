import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';
import { addNewClue, removeClue, hideDetail } from './../../events/actions/action-app';
import { isMacOSSafari } from '../../utils/support';

import gallery_35_small from './../../media/gallery/gallery_35.jpg';
import gallery_36_small from './../../media/gallery/gallery_36.jpg';



class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            message: `
<h1>Karlexa <span>(2018)</span></h1>
<p>
</p>
`
        };
    }

    toggleFullScreenState = (event) => {
        this.setState({
            fullScreen : !this.state.fullScreen,
        });
        if (this.state.fullScreen) {
            document.querySelector('#app').classList.add(this.context.styles['lock']);
        } else {
            document.querySelector('#app').classList.remove(this.context.styles['lock']);
        }
    };

    componentDidMount() {
        document.addEventListener("webkitfullscreenchange", this.toggleFullScreenState);

        document.querySelector('#app').className = this.context.styles['horizontal'];
        // setTimeout(() => {
        //     this.props.addNewClue({
        //         pk: 'karlexa-0',
        //         type: 'note',
        //         position: {
        //             x: 500,
        //             y: 50
        //         },
        //         angle: 2.5,
        //         message: this.state.message
        //     });
        //
        //
        //
        //     this.props.addNewClue({
        //         pk: 'karlexa-2',
        //         type: 'photo',
        //         position: {
        //             x: -475,
        //             y: 43
        //         },
        //         angle: -5,
        //         url: gallery_35_small,
        //         caption: 'Outcome story 1'
        //     });
        //
        //     this.props.addNewClue({
        //         pk: 'karlexa-1',
        //         type: 'photo',
        //         position: {
        //             x: -351,
        //             y: 226
        //         },
        //         angle: 3,
        //         url: gallery_36_small,
        //         caption: 'Outcome story 2'
        //     });
        //
        // }, 1000);
    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
        this.props.removeClue(['karlexa-0', 'karlexa-1', 'karlexa-2']);
        this.props.hideDetail();

        document.removeEventListener("webkitfullscreenchange", this.toggleFullScreenState);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <iframe className={this.state.fullScreen ? null : this.context.styles['rotate']}
                        width="644" height="404"
                        src="https://www.youtube.com/embed/6CTO2XeQngg?rel=0" frameBorder="0"
                        allow="autoplay; encrypted-media" allowFullScreen></iframe>
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addNewClue, removeClue, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
