import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';

import { isDesktopBrowser } from '../../utils/support';
import { bringClueFront, addNewClue, removeClue, markTaskDone, hideDetail } from './../../events/actions/action-app';

import gallery_22_small from './../../media/gallery/gallery_22_small.jpg';
import gallery_23_small from './../../media/gallery/gallery_23_small.jpg';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.iframe = React.createRef();
        this.state = {
            message: `
<h1>Schrodinger's Cat <span>(2014)</span></h1>
<p>
A tragic story about a helpless girl who tries to survive in a post-apocalyptic world. Readers play as a girl, name <strong>Gilly</strong>, experience a harsh situation in a destroyed city, and make decisions to survive with her mother, Annemarie.<br/><br/>In the early part of the story, Gilly, as a 16 year-old girl, is naïve and dependent on her mother. As the story advances, Gilly gains more hope of her father’s survival, and becomes more self-reliant, and readers experience Gilly’s grown up together. However, as she becomes more independent, she cannot have a choice but to know the truth that she wouldn’t want to know.</p>
<p>
This story is made using <a href="https://github.com/captainwhale52/schrodinger-s_cat" target="_blank">Captain Story Engine</a>, designed for creating an interactive narrative story using choice and node structure. The engine comes with a mobile-friendly layout as well as an automatic progress save feature so that readers can come back anytime with any device to continue the story.
</p>

<h1>Dev Team:</h1>
<p>
<strong>Writer: </strong> Karl Kim<br/>
<strong>Art: </strong> Karl Kim<br/>
<strong>Design & Dev: </strong> Karl Kim<br/>
</p>
`
        };
    }

    componentDidMount() {
        this.props.bringClueFront(2);
        this.props.markTaskDone(5);
        setTimeout(() => {
            this.props.addNewClue({
                pk: 'schrodingerscat-0',
                type: 'note',
                position: {
                    x: 500,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message
            });

            this.props.addNewClue({
                pk: 'schrodingerscat-1',
                type: 'photo',
                position: {
                    x: -297,
                    y: 300
                },
                angle: -5,
                url: gallery_23_small,
                caption: 'Chapter 3 cover image'
            });

            this.props.addNewClue({
                pk: 'schrodingerscat-2',
                type: 'photo',
                position: {
                    x: -517,
                    y: 387
                },
                angle: 3,
                url: gallery_22_small,
                caption: 'Chapter 1 cover image'
            });



        }, 250);

        if (isDesktopBrowser()) {
           this.iframe.current.childNodes[1].contentWindow.focus()
        }
    }

    componentWillUnmount() {
        this.props.removeClue(['schrodingerscat-0', 'schrodingerscat-1', 'schrodingerscat-2']);
        this.props.hideDetail();
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <iframe src={`${process.env.PHPS_BASE_URL}/schrodinger-s-cat/`} />
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addNewClue, removeClue, markTaskDone, bringClueFront, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
