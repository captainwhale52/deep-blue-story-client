import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from './message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';

import { isDesktopChromeBrowser, isMacOSSafari } from '../../utils/support';
import { bringClueFront, markTaskDone } from "./../../events/actions/action-app";
import { addNewClue, removeClue, hideDetail } from './../../events/actions/action-app';

import gallery_28_small from './../../media/gallery/gallery_28_small.jpg';
import gallery_29_small from './../../media/gallery/gallery_29_small.jpg';
import gallery_30_small from './../../media/gallery/gallery_30_small.jpg';



class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            message: `
<h1>The Last Goodbye <span>(2018)</span></h1>
<p>
A story about a girl, Shirley Green, who visits Gastown in Vancouver, to meet her boy friend, Steven Roy.
</p>
<p>
It is a transmedia storytelling project across <strong>Unity</strong> and <strong>Instagram</strong>. Viewers can leave a message to Shirley from the perspective of Steven at the end of the film, and the text message is compiled with one of polaroid photos in the film and posted on Instagram. After watching the film, viewers can visit the Instagram page <a href="https://www.instagram.com/love_in_vancouver/?hl=en" target="_blank">@love_in_vancouver</a> and share their thoughts with other people who also watched the film.
</p>

<h1>Dev Team:</h1>
<p>
<strong>Director: </strong>Karl Kim, Celine Li<br/>
<strong>Writer: </strong>Celine Li<br/>
<strong>Producer: </strong>Karl Kim<br/>
<strong>Programming: </strong>Karl Kim<br/>
<strong>Stars: </strong>Annita Chow, Daniel James<br/>
<strong>Cast & Crew: </strong>So Jeong Bae, Samantha Yueh, Khushiboo Vashisht, Fei Wang, Ying Grape, Eason Pan, Stephanie Oey, Jorge Luis Taracena<br/>
</p>
`
        };
    }

    toggleFullScreenState = (event) => {
        this.setState({
            fullScreen : !this.state.fullScreen,
        });
        if (this.state.fullScreen) {
            document.querySelector('#app').classList.add(this.context.styles['lock']);
        } else {
            document.querySelector('#app').classList.remove(this.context.styles['lock']);
        }
    };

    componentDidMount() {
        document.addEventListener("webkitfullscreenchange", this.toggleFullScreenState);

        document.querySelector('#app').className = this.context.styles['horizontal'];
        this.props.bringClueFront(2);
        this.props.markTaskDone(3);

        setTimeout(() => {
            this.props.addNewClue({
                pk: 'thelastgoodbye-0',
                type: 'note',
                position: {
                    x: 550,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message
            });

            this.props.addNewClue({
                pk: 'thelastgoodbye-1',
                type: 'photo',
                position: {
                    x: -163,
                    y: 641
                },
                angle: -10,
                url: gallery_28_small,
                caption: 'Shirley Green',
            });

            this.props.addNewClue({
                pk: 'thelastgoodbye-2',
                type: 'photo',
                position: {
                    x: 56,
                    y: -11
                },
                angle: -5,
                url: gallery_29_small,
                caption: 'Steven & Shirley',
            });

            this.props.addNewClue({
                pk: 'thelastgoodbye-3',
                type: 'photo',
                position: {
                    x: 308,
                    y: 637
                },
                angle: 10,
                url: gallery_30_small,
                caption: 'Shirley & Tourists',
            });


        }, 500);
    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
        this.props.removeClue(['thelastgoodbye-0', 'thelastgoodbye-1', 'thelastgoodbye-2', 'thelastgoodbye-3']);
        this.props.hideDetail();

        document.removeEventListener("webkitfullscreenchange", this.toggleFullScreenState);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <iframe className={this.state.fullScreen ? null : this.context.styles['rotate']}
                        width="644" height="404"
                        src="https://www.youtube.com/embed/uiYf3c1tPjo?rel=0" frameBorder="0"
                        allow="autoplay; encrypted-media" allowFullScreen></iframe>
                <Footer history={this.props.history} />
            </div>
        );

        // if (process.env['NODE_ENV'] === 'production' && isDesktopChromeBrowser()) {
        //     return (
        //         <div className={this.context.styles['iframe-app']}>
        //             <Header />
        //             <iframe className={this.context.styles['rotate']} src={`${process.env.PHPS_BASE_URL}/the-last-good-bye/`} width="644px" height="404px" />
        //             <Footer history={this.props.history} />
        //         </div>
        //     );
        // } else {
        //
        // }

    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        bringClueFront, markTaskDone, addNewClue, removeClue, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
