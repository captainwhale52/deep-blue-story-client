import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import {isDesktopChromeBrowser} from "../../utils/support";
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';
import { showDetail, hideDetail, addNewClue, removeClue } from './../../events/actions/action-app';
import { isMacOSSafari } from '../../utils/support';

import gallery_25_small from './../../media/gallery/gallery_25_small.jpg';
import gallery_26_small from './../../media/gallery/gallery_26_small.jpg';
import gallery_16_small from './../../media/gallery/gallery_16_small.jpg';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            message: `
<h1>Stay <span>(2014)</span></h1>
<p>A story about a girl, Emily White, and a guy, Albert Fratellini, who are both having a trouble of socializing themselves for different reasons, but able to communicate with each other via a small drawing.</p>
<h1>Dev Team:</h1>
<p>
<strong>Director & Writer: </strong>Karl Kim<br/>
<strong>Stars: </strong>Tien Troung, Svyatoslav Kucheryavykh<br/>
<strong>Cast & Crew: </strong>Jordan Goodall, Tre'Saun Thomas, Thomas Bjarke Heiberg-Iürgensen, Aida Yoguely, Aron Tannenbaum, Julian Popescu, Luisito Rivera, Nam Cuong<br/>
</p>`
        };
    }

    toggleFullScreenState = (event) => {
        this.setState({
            fullScreen : !this.state.fullScreen,
        });
        if (this.state.fullScreen) {
            document.querySelector('#app').classList.add(this.context.styles['lock']);
        } else {
            document.querySelector('#app').classList.remove(this.context.styles['lock']);
        }
    };

    componentDidMount() {
        document.addEventListener("webkitfullscreenchange", this.toggleFullScreenState);

        document.querySelector('#app').className = this.context.styles['horizontal'];
        setTimeout(() => {
            this.props.addNewClue({
                pk: 'stay-0',
                type: 'note',
                position: {
                    x: 550,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message
            });

            this.props.addNewClue({
                pk: 'stay-1',
                type: 'photo',
                position: {
                    x: -150,
                    y: 25
                },
                angle: -10,
                url: gallery_25_small,
                caption: 'Albert Fratellini'
            });

            this.props.addNewClue({
                pk: 'stay-2',
                type: 'photo',
                position: {
                    x: -380,
                    y: 625
                },
                angle: 10,
                url: gallery_26_small,
                caption: 'Emily White'
            });

            this.props.addNewClue({
                pk: 'stay-3',
                type: 'photo',
                position: {
                    x: 180,
                    y: 635
                },
                angle: -10,
                url: gallery_16_small,
                caption: 'Last scene'
            });
        }, 500);

    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
        this.props.removeClue(['stay-0', 'stay-1', 'stay-2', 'stay-3']);
        this.props.hideDetail();

        document.removeEventListener("webkitfullscreenchange", this.toggleFullScreenState);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <iframe className={this.state.fullScreen ? null : this.context.styles['rotate']}
                        width="644" height="404"
                        src="https://www.youtube.com/embed/TbxKIWhTMkU?rel=0" frameBorder="0"
                        allow="autoplay; encrypted-media" allowFullScreen></iframe>
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        showDetail, hideDetail, addNewClue, removeClue, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
