import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
// import GalleryGroup from './gallery-group';
// import MessageThread from './message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';

import { isDesktopChromeBrowser, isMacOSSafari } from '../../utils/support';

import { bringClueFront } from "./../../events/actions/action-app";
import { addNewClue, removeClue, markTaskDone, hideDetail } from './../../events/actions/action-app';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            message: `
<h1>MyMusicMate <span>(2017)</span></h1>
<p>
a friendly <strong>chatbot</strong> for Slack that helps you and your friends find a concert to go together based on your collective music tastes, built for the <a href="https://devpost.com/software/mymusicmate-42xcyv" target="_blank">AWS Chatbot Challenge 2017</a>.
</p>
<p>
It's built with: <strong>Amazon Lex</strong>, <strong>DynamoDB</strong>, <strong>SNS</strong>, <strong>S3</strong>, <strong>API Gateway</strong>, and <strong>Lambda</strong> with the Serverless Framework handling the deployment; BandsInTown, LastFM, and YouTube APIs for retrieval of concerts, artist information, and concert videos, respectively; and finally Slack, for putting the chat in chatbot.
</p>

<h1>What it does:</h1>
<p>
MyMusicMate starts by rounding up your concert-loving friends into a Slack channel and asking about everyone's music tastes. Folks can name artists they like, or they can simply name genres if they don't have any specific artists in mind. MyMusicMate leverages the LastFM API to help get artist recommendations based off of user input, and then asks users to specify where they're interested in going to a concert. For now, MyMusicMate only focuses on cities within the US, but it's learning more with every passing day/commit ;)<br/><br/>
MyMusicMate then takes this information and uses the BandsInTown Concert Search API to find upcoming events and the YouTube Search API to find live concert videos to help users make their decision. If you're unsure about whether or not you'd like to see an artist in person, maybe a video of a live concert could help!<br/><br/>
Finally, MyMusicMate takes the list of concert results and concert videos and displays them in the Slack channel, allowing users to mull their decision over and place a vote for which concert the group should go to. And don't worry - if your group can't agree on a concert, MyMusicMate will be happy to offer new suggestions or get new musical preferences until it can find what's right for you!
</p>

<h1>Dev Team:</h1>
<p>
<strong>Jinwook Baek</strong> - CTO at MyMusicTaste<br/>
<strong>Seokhui Hong</strong> - Product Manager at MyMusicTaste<br/>
<strong>Jaekeon Hwang</strong> - Infra Engineer at MyMusicTaste<br/>
<strong>Paul Elliot</strong> - Data Team Manager at MyMusicTaste<br/>
<strong>Jonathon Son</strong> - Data Engineer at MyMusicTaste<br/>
<strong>Taeheon Ki</strong> - iOS Developer at MyMusicTaste<br/>
<strong>Moonju Kang</strong> - Designer at MyMusicTaste<br/>
<strong>Karl Kim</strong> - FrontEnd Developer at MyMusicTaste<br/>
</p>
`
        };
    }

    toggleFullScreenState = (event) => {
        this.setState({
            fullScreen : !this.state.fullScreen,
        });
        if (this.state.fullScreen) {
            document.querySelector('#app').classList.add(this.context.styles['lock']);
        } else {
            document.querySelector('#app').classList.remove(this.context.styles['lock']);
        }
    };

    componentDidMount() {

        document.addEventListener("webkitfullscreenchange", this.toggleFullScreenState);

        document.querySelector('#app').className = this.context.styles['horizontal'];
        this.props.bringClueFront(2);
        this.props.markTaskDone(4);

        setTimeout(() => {
            this.props.addNewClue({
                pk: 'mymusicmate-0',
                type: 'note',
                position: {
                    x: 550,
                    y: 50
                },
                angle: 2.5,
                message: this.state.message,
            });

            // this.props.addNewClue({
            //     pk: 'mymusicmate-1',
            //     type: 'photo',
            //     position: {
            //         x: 644,
            //         y: 580
            //     },
            //     angle: -10,
            //     url: gallery_5_small,
            //     caption: 'Gilly face texturing',
            // });


        }, 500);
    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
        this.props.removeClue(['mymusicmate-0', 'mymusicmate-1', 'mymusicmate-2', 'mymusicmate-3', 'mymusicmate-4']);
        this.props.hideDetail();

        document.removeEventListener("webkitfullscreenchange", this.toggleFullScreenState);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (this.props.detail) {
            return <div ref={this.iframe} className={this.context.styles['iframe-app']}>
                <Header />
                <div className={this.context.styles['detail']}><div dangerouslySetInnerHTML={{__html: this.state.message}} /></div>
                <Footer history={this.props.history} />
            </div>
        }
        return (
            <div className={this.context.styles['iframe-app']}>
                <Header />
                <iframe className={this.state.fullScreen ? null : this.context.styles['rotate']}
                        width="644" height="404"
                        src="https://www.youtube.com/embed/wf5HKRkPlbY?rel=0" frameBorder="0"
                        allow="autoplay; encrypted-media" allowFullScreen></iframe>
                <Footer history={this.props.history} />
            </div>
        );

        // if (process.env['NODE_ENV'] === 'production' && isDesktopChromeBrowser()) {
        //     return (
        //         <div className={this.context.styles['iframe-app']}>
        //             <Header />
        //             <iframe className={this.context.styles['rotate']} src={`${process.env.PHPS_BASE_URL}/the-last-good-bye/`} width="644px" height="404px" />
        //             <Footer history={this.props.history} />
        //         </div>
        //     );
        // } else {
        //
        // }

    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        bringClueFront, addNewClue, removeClue, markTaskDone, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
