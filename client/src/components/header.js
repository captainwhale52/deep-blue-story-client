import React from 'react';
import PropTypes from "prop-types";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import moment from 'moment';

import GLOBALS from './../globals/global-index';

import image_battery from './../media/battery.png';
import image_wifi_1 from './../media/wifi-1.png';
import image_wifi_2 from './../media/wifi-2.png';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            time: moment().format('h : mm A'),
            wifi: image_wifi_1,
        };
    }

    componentDidMount() {
        this.updateTime = setInterval(() => {
            this.setState({
                time: moment().format('h : mm A'),
            });
        }, 1000);
        this.updateWifiSignal();
    }

    updateWifiSignal = () => {
        this.updateWifi = setTimeout(() => {
            if (this.state.wifi === image_wifi_1) {
                this.setState({
                    wifi: image_wifi_2,
                });
            } else {
                this.setState({
                    wifi: image_wifi_1,
                });
            }
            this.updateWifiSignal();
        }, Math.random() * 5000 + 2500)
    };

    componentWillUnmount() {
        clearInterval(this.updateTime);
        clearInterval(this.updateWifi);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {};
    }

    render() {
        return (
            <header className={this.context.styles['header']}>
                <div>
                    <img src={this.state.wifi}/>
                </div>
                <div className={this.context.styles['current-time']}>
                    {this.state.time}
                </div>
                <div>
                    <img src={image_battery}/>
                </div>
            </header>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
