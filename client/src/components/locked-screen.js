import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from './../globals/global-index';
import { unlock, bringClueFront, removeClue, addNewClue } from './../events/actions/action-app';

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            time: moment().format('h:mm A'),
            date: moment().format('MMMM Do YYYY'),
            passCode: [null, null, null, null],
        };
    }

    componentDidMount() {
        this.props.addNewClue({
            pk: 1,
            type: 'clue',
            position: {
                x: -403,
                y: 87
            },
            angle: -8,
            message: `Karl,<br/><br/>Welcome back!<br/><br/>I finally found your phone from the water, and reset the <span>password</span> as your <span>name</span>.<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Eason`,
            tasks: []
        });

        // this.props.bringClueFront(1);

        this.setState({
            passCode: [null, null, null, null],
        });
        this.updateTime = setInterval(() => {
            this.setState({
                time: moment().format('h:mm A'),
                date: moment().format('MMMM Do YYYY'),
            });
        }, 1000);
        // this.props.bringClueFront(1);
    }

    componentWillUnmount() {
        clearInterval(this.updateTime);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    pressKeyCode = (code) => {
        const passCode = this.state.passCode;
        if (passCode[0] === null) {
            passCode[0] = code;
        } else if (passCode[1] === null) {
            passCode[1] = code;
        } else if (passCode[2] === null) {
            passCode[2] = code;
        } else if (passCode[3] === null) {
            passCode[3] = code;

            if (passCode.join('') === this.props.passCode) {
                this.props.unlock();
                this.props.removeClue([1]);
                setTimeout(() => {
                    this.props.history.push('/home');
                }, 1500);
            } else {
                setTimeout(() => {
                    this.setState({
                        passCode: [null, null, null, null],
                    });
                }, 500);
            }
        }
        this.setState({
            passCode: passCode
        });


    };

    render() {
        let classHide = this.context.styles['hide'];
        const passCode = this.state.passCode.map((code, index) => {
            if (code !== null) {
                return <div className={this.context.styles['valid']} key={index}>*</div>;
            }
            classHide = '';
            return <div key={index}>_</div>;
        });

        return (
            <div className={`${this.context.styles['locked-screen']} ${classHide}`}>
                <div className={this.context.styles['current-time']}>
                    {this.state.time}
                </div>
                <div className={this.context.styles['current-date']}>
                    {this.state.date}
                </div>
                <div className={this.context.styles['pressed-numbers']}>
                    {passCode}
                </div>
                <div className={this.context.styles['number-pads']}>
                    <div onClick={() => {this.pressKeyCode(1)}}>
                        <div className={this.context.styles['numbers']}>1</div>
                        <div className={this.context.styles['letters']}><span className={this.context.styles['highlight1']}>A</span><span>BC</span></div>
                    </div>
                    <div onClick={() => {this.pressKeyCode(2)}}>
                        <div className={this.context.styles['numbers']}>2</div>
                        <div className={this.context.styles['letters']}>DEF</div>
                    </div>
                    <div onClick={() => {this.pressKeyCode(3)}}>
                        <div className={this.context.styles['numbers']}>3</div>
                        <div className={this.context.styles['letters']}>GHI</div>
                    </div>
                </div>
                <div className={this.context.styles['number-pads']}>
                    <div onClick={() => {this.pressKeyCode(4)}}>
                        <div className={this.context.styles['numbers']}>4</div>
                        <div className={this.context.styles['letters']}><span>J</span><span className={this.context.styles['highlight2']}>K</span><span className={this.context.styles['highlight3']}>L</span></div>
                    </div>
                    <div onClick={() => {this.pressKeyCode(5)}}>
                        <div className={this.context.styles['numbers']}>5</div>
                        <div className={this.context.styles['letters']}>MNO</div>
                    </div>
                    <div onClick={() => {this.pressKeyCode(6)}}>
                        <div className={this.context.styles['numbers']}>6</div>
                        <div className={this.context.styles['letters']}><span>PQ</span><span className={this.context.styles['highlight4']}>R</span></div>
                    </div>
                </div>
                <div className={this.context.styles['number-pads']}>
                    <div onClick={() => {this.pressKeyCode(7)}}>
                        <div className={this.context.styles['numbers']}>7</div>
                        <div className={this.context.styles['letters']}>STU</div>
                    </div>
                    <div onClick={() => {this.pressKeyCode(8)}}>
                        <div className={this.context.styles['numbers']}>8</div>
                        <div className={this.context.styles['letters']}>VWX</div>
                    </div>
                    <div onClick={() => {this.pressKeyCode(9)}}>
                        <div className={this.context.styles['numbers']}>9</div>
                        <div className={this.context.styles['letters']}>YNZ</div>
                    </div>
                </div>
                <div className={this.context.styles['number-pads']}>
                    <div className={this.context.styles['empty-pad']} />
                    <div onClick={() => {this.pressKeyCode(0)}}>0</div>
                    <div className={this.context.styles['empty-pad']} />
                </div>
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        passCode: state.app.passCode
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        unlock, bringClueFront, removeClue, addNewClue
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
