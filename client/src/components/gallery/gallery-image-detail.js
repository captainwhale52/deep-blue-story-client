import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';


import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import { messageDateTimeFormat } from './../../utils/formatter';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {
        document.querySelector('#app').className = this.context.styles['horizontal'];
    }

    componentWillUnmount() {
        document.querySelector('#app').className = '';
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        const images = this.props.images.filter((image, index) => {
            return parseInt(this.props.match.params.imageId) === image.pk;
        });
        return (
            <main>
                <div className={this.context.styles['app-name']}>
                    <div className={this.context.styles['menu']} onClick={() => {
                    this.props.history.goBack();
                }}>{`<`}</div>
                    <div>{images[0].title}</div>
                    <div></div>
                </div>
                <div className={`${this.context.styles['container']} ${this.context.styles['image-detail']}`}>
                    <img src={images[0].url} />
                </div>
            </main>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        images: state.gallery.images
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
