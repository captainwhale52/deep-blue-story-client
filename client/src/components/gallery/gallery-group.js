import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';
import GalleryGroupItem from './gallery-group-item';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        const images = this.props.images.map((image, index) => {
            return <GalleryGroupItem key={`group-${index}`} history={this.props.history} image={image} />
        });

        return (
            <main>
                <div className={this.context.styles['app-name']}>
                    <div></div>
                    <div>GALLERY</div>
                    <div></div>
                </div>
                <div className={this.context.styles['container']}>
                    <div>
                        {images}
                    </div>
                </div>
            </main>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        images: state.gallery.images
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
