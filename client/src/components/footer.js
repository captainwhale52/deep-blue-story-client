import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from './../globals/global-index';

import image_home_button from './../media/home-button.png';
import image_back_button from './../media/back-button.png';
import image_info_button from './../media/info-button.png';
import image_close_button from './../media/close-button.png';

import store from "../events/stores/store-index";
import {isDesktopBrowser} from "../utils/support";

import { showDetail, hideDetail } from "../events/actions/action-app";

class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (!isDesktopBrowser() && window.location.pathname !== '/home') {
            if (this.props.detail) {
                 return (
                    <footer className={this.context.styles['footer']}>
                        <div><div className={this.context.styles['back']} onClick={() => {
                            this.props.hideDetail();
                        }}><img src={image_back_button} /></div></div>

                        <div><div className={this.context.styles['home']} onClick={() => {
                            this.props.history.push('/home');
                        }}><img src={image_home_button} /></div></div>

                        <div><div className={this.context.styles['info']} onClick={() => {
                            // if (!window.location.pathname.includes('/info')) {
                            //     this.props.history.push(`${window.location.pathname}/info`);
                            //     this.props.showDetail();
                            // }
                            this.props.hideDetail();
                        }}><img src={image_close_button} /></div></div>

                    </footer>
                );
            } else {
                return (
                    <footer className={this.context.styles['footer']}>
                        <div><div className={this.context.styles['back']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.goBack();
                        }}><img src={image_back_button} /></div></div>

                        <div><div className={this.context.styles['home']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/home');
                        }}><img src={image_home_button} /></div></div>

                        <div><div className={this.context.styles['info']} onClick={() => {
                            // if (!window.location.pathname.includes('/info')) {
                            //     this.props.history.push(`${window.location.pathname}/info`);
                            //     this.props.showDetail();
                            // }
                            this.props.showDetail();
                        }}><img src={image_info_button} /></div></div>

                    </footer>
                );
            }
        }
        return (
            <footer className={this.context.styles['footer']}>
                <div><div className={this.context.styles['back']} onClick={() => {
                    this.props.history.goBack();
                }}><img src={image_back_button} /></div></div>
                <div><div className={this.context.styles['home']} onClick={() => {
                    this.props.history.push('/home');
                }}><img src={image_home_button} /></div></div>
                <div></div>
            </footer>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        showDetail, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
