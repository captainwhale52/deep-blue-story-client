import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import moment from 'moment';
import BigCalendar from 'react-big-calendar';

import GLOBALS from '../../globals/global-index';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
        BigCalendar.momentLocalizer(moment);
    }

    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        return (
            <main>
                <div className={this.context.styles['app-name']}>
                    <div></div>
                    <div>CALENDAR</div>
                    <div></div>
                </div>
                <div className={this.context.styles['container']}>
                    <BigCalendar
                        views={['month', 'day', 'agenda']}
                        events={[
                            {
                                id: 0,
                                title: 'All Day Event',
                                allDay: true,
                                start: new Date(2018, 4, 22),
                                end: new Date(2018, 4, 25),
                            },
                        ]}
                    />
                </div>
            </main>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
