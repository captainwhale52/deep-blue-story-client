import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';
import Draggable from 'react-draggable';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import {isDesktopChromeBrowser} from "../../utils/support";
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';
import { bringClueFront } from '../../events/actions/action-app';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isAnimating: false,
            isAnimationFinished: false,
            isDeactivating: false,
            isPicking: false,
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                isAnimating: true,
            });
        }, 500);
        setTimeout(() => {
            this.setState({
                isAnimationFinished: true,
            });
        }, 2000);
    }

    componentWillUnmount() {

    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.photo.deactivated && !this.state.isDeactivating) {
            setTimeout(() => {
                this.setState({
                    isDeactivating: true,
                });
            }, 10);
        }
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //
    //     return {
    //
    //     };
    // }

    handleDragStart = () => {
        this.props.bringClueFront(this.props.photo.pk);
        this.setState({
            isPicking: true,
        });
    };

    handleDragEnd = () => {
        this.setState({
            isPicking: false,
        });
    };

    render() {
        const classPick = this.state.isPicking ? this.context.styles['pick'] : '';
        if (this.state.isDeactivating) {
            return (
                <div className={this.context.styles['animation-card']}  style={{
                    top: 0,
                    left: '-75vw',
                    transform: `rotate(-90deg)`
                }}>
                    <div>
                        <div className={`${this.context.styles['photo']}`} style={{
                            transform: `rotate(${this.props.photo.angle}deg)`
                        }}>
                            <img src={this.props.photo.url} />
                        </div>
                    </div>
                </div>
            )
        } else if (this.props.photo.deactivated) {
            return (
                <div className={this.context.styles['animation-card']}  style={{
                    top: this.props.photo.position.y,
                    left: this.props.photo.position.x,
                    transform: `rotate(0deg)`
                }}>
                    <div>
                        <div className={`${this.context.styles['photo']}`} style={{
                            transform: `rotate(${this.props.photo.angle}deg)`
                        }}>
                            <img src={this.props.photo.url} />
                            <div>{this.props.photo.caption}</div>
                        </div>
                    </div>
                </div>
            )
        } else if (this.state.isAnimationFinished) {
            return (
                <Draggable
                    defaultPosition={{x: this.props.photo.position.x, y: this.props.photo.position.y}}
                    onStart={this.handleDragStart}
                    onStop={this.handleDragEnd}
                >
                    <div>
                        <div className={`${this.context.styles['photo']} ${classPick}`} style={{
                            transform: `rotate(${this.props.photo.angle}deg)`
                        }}>
                            <img src={this.props.photo.url} />
                            <div>{this.props.photo.caption}</div>
                        </div>
                    </div>
                </Draggable>
            );
        } else if (this.state.isAnimating) {
            return (
                <div className={this.context.styles['animation-card']} style={{
                    top: this.props.photo.position.y,
                    left: this.props.photo.position.x,
                    transform: `rotate(0deg)`
                }}>
                    <div>
                        <div className={`${this.context.styles['photo']}`} style={{
                            transform: `rotate(${this.props.photo.angle}deg)`
                        }}>
                            <img src={this.props.photo.url} />
                            <div>{this.props.photo.caption}</div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className={this.context.styles['animation-card']}>
                    <div>
                        <div className={`${this.context.styles['photo']}`} style={{
                            transform: `rotate(${this.props.photo.angle}deg)`
                        }}>
                            <img src={this.props.photo.url} />
                            <div>{this.props.photo.caption}</div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        bringClueFront
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
