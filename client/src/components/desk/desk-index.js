import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import ClueItem from './clue-item';
import NoteItem from './note-item';
import PhotoItem from './photo-item';



class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        const clues = this.props.clues.map((clue, index) => {
            if (clue.type === 'clue') {
                return <ClueItem key={`clue-${clue.pk}`} clue={clue}/>
            } else if (clue.type === 'note') {
                return <NoteItem key={`clue-${clue.pk}`} note={clue}/>
            } else if (clue.type === 'photo') {
                return <PhotoItem key={`clue-${clue.pk}`} photo={clue}/>
            }
        });

        // let note, photos;
        // if (this.props.detail) {
        //     if (this.props.detail.note) {
        //         note = <NoteItem note={this.props.detail.note} />
        //     }
        //     if (this.props.detail.photos) {
        //         photos = this.props.detail.photos.map((photo, index) => {
        //            return <PhotoItem key={`photo-${index}`} photo={photo}/>
        //         });
        //     }
        // }



        return (
            <div className={this.context.styles['desk']}>
                {clues}
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        clues: state.app.clues,
        // detail: state.app.detail,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
