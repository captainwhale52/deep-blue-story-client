import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';
import Draggable from 'react-draggable';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import {isDesktopChromeBrowser} from "../../utils/support";
// import GalleryGroup from './gallery-group';
// import MessageThread from '../message/message-thread';
// import GalleryImageDetail from './gallery-image-detail';

import { bringClueFront } from "./../../events/actions/action-app";


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isPicking: false
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    handleDragStart = () => {
        this.props.bringClueFront(this.props.clue.pk);
        this.setState({
            isPicking: true,
        });
    };

    handleDragEnd = () => {
        this.setState({
            isPicking: false,
        });
    };

    render() {
        const classPick = this.state.isPicking ? this.context.styles['pick'] : '';
        const tasks = this.props.clue.tasks.map((task, index) => {
            if (task.done) {
                return <li key={`task-${task.pk}`} className={this.context.styles['done']} dangerouslySetInnerHTML={{__html: task.text}} />
            }
            return <li key={`task-${task.pk}`} dangerouslySetInnerHTML={{__html: task.text}} />
        });

        return (
            <Draggable
                defaultPosition={{x: this.props.clue.position.x, y: this.props.clue.position.y}}
                onStart={this.handleDragStart}
                onStop={this.handleDragEnd}
            >
                <div>
                    <div className={`${this.context.styles['memo']} ${classPick}`} style={{
                        transform: `rotate(${this.props.clue.angle}deg)`
                    }}>
                        <div>
                            <div dangerouslySetInnerHTML={{__html: this.props.clue.message}} />
                            {tasks}
                        </div>
                    </div>
                </div>
            </Draggable>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        bringClueFront
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
