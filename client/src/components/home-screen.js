import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from './../globals/global-index';

import Header from './header';
import Footer from './footer';
import image_message from './../media/message-icon.png';
import image_calendar from './../media/calendar-icon.png';
import image_gallery from './../media/gallery-icon.png';
import image_schrodingers_cat from './../media/schrodinger-s-cat-icon.png';
import iconTheLastGoodbye from './../media/the-last-goodbye-icon.png';
import iconInstagram from './../media/instagram-icon.png';
import iconStay from './../media/stay-icon.png';
import iconNotes from './../media/notes-icon.png';
import iconGilly from './../media/gilly-icon.png';
import iconFoodParent from './../media/foodparent-icon.png';
import iconEscapeGoat2 from './../media/escape-goat-2-icon.png';
import iconMyMusicMate from './../media/mmm-icon.png';
import iconSmartCity from './../media/smart-city-icon.png';
import iconLinkedIn from './../media/linked-in-icon.png';
import iconUmlaut from './../media/u-umlaut-icon.png';
import iconKarlexa from './../media/karlexa-icon.png';


import { isDesktopChromeBrowser, isDesktopBrowser, isIOS } from './../utils/support';
import { removeClue, bringClueFront, hideDetail } from './../events/actions/action-app';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        if (isDesktopBrowser() && window.location.pathname !== '/home') {
            return null;
        }

        return (
            <div className={this.context.styles['home-screen']}>
                <Header />
                <main>
                    <div className={this.context.styles['container']}>

                        <div className={this.context.styles['app']} onClick={() => {
                            const tab = window.open(process.env.LINKEDIN_URL, '_blank');
                            tab.focus();
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['blue']}`}>
                                <img src={iconLinkedIn} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                LinkedIn
                            </div>
                        </div>



                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/my-music-mate');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['red']}`}>
                                <img src={iconMyMusicMate} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                MyMusicMate
                            </div>
                        </div>

                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/escape-goat-2');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['yellow']}`}>
                                <img src={iconEscapeGoat2} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                Escape Goat 2
                            </div>
                        </div>

                        <div className={this.context.styles['app']} onClick={() => {
                            // if (!isDesktopChromeBrowser()) {
                            //     alert(`Your browser doesn't support Unity WebGL. If you want a full experience, please use a Mac/Windows Chrome browser.`);
                            // }
                            this.props.hideDetail();
                            this.props.history.push('/the-last-goodbye');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['cyan']}`}>
                                <img src={iconTheLastGoodbye} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                The Last Goodbye
                            </div>
                        </div>

                        {/*<div className={this.context.styles['app']} onClick={() => {*/}
                            {/*// this.props.history.push('/instagram');*/}
                            {/*const instagram = window.open(process.env.INSTAGRAM_URL, '_blank');*/}
                            {/*instagram.focus();*/}
                        {/*}}>*/}
                            {/*<div className={`${this.context.styles['app-icon']} ${this.context.styles['gray-4']}`}>*/}
                                {/*<img src={iconInstagram} />*/}
                            {/*</div>*/}
                            {/*<div className={this.context.styles['app-name']}>*/}
                                {/*@love_in_<br/>vancouver*/}
                            {/*</div>*/}
                        {/*</div>*/}

                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/stay');
                            // const instagram = window.open(process.env.INSTAGRAM_URL, '_blank');
                            // instagram.focus();
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['cyan']}`}>
                                <img src={iconStay} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                Stay
                            </div>
                        </div>

                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/gilly-story');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['purple']}`}>
                                <img src={iconGilly} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                Gilly's Story
                            </div>
                        </div>

                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            if (isIOS()) {
                                const scat = window.open(`${process.env.PHPS_BASE_URL}/schrodinger-s-cat/`, '_blank');
                                scat.focus();
                            } else {
                                this.props.history.push('/schrodinger-s-cat');
                            }

                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['purple']}`}>
                                <img src={image_schrodingers_cat} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                Schrodinger's Cat
                            </div>
                        </div>

                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/u-umlaut');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['blue']}`}>
                                <img src={iconUmlaut} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                U-umlaut
                            </div>
                        </div>




                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/food-parent');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['green']}`}>
                                <img src={iconFoodParent} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                Food Parent
                            </div>
                        </div>



                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/atlanta-smart-city');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['gray-4']}`}>
                                <img src={iconSmartCity} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                Atlanta Smart City
                            </div>
                        </div>

                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/message/1');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['red']}`}>
                                <img src={image_message} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                AI Chatbot
                            </div>
                        </div>

                        <div className={this.context.styles['app']} onClick={() => {
                            this.props.hideDetail();
                            this.props.history.push('/karlexa');
                        }}>
                            <div className={`${this.context.styles['app-icon']} ${this.context.styles['gray-4']}`}>
                                <img src={iconKarlexa} />
                            </div>
                            <div className={this.context.styles['app-name']}>
                                Karlexa
                            </div>
                        </div>



                        {/*<div className={this.context.styles['app']} onClick={() => {*/}
                            {/*this.props.history.push('/calendar');*/}
                        {/*}}>*/}
                            {/*<div className={`${this.context.styles['app-icon']} ${this.context.styles['green']}`}>*/}
                                {/*<img src={image_calendar} />*/}
                            {/*</div>*/}
                            {/*<div className={this.context.styles['app-name']}>*/}
                                {/*Calendar*/}
                            {/*</div>*/}
                        {/*</div>*/}













                        {/*<div className={this.context.styles['app']} onClick={() => {*/}
                            {/*this.props.history.push('/notes');*/}
                        {/*}}>*/}
                            {/*<div className={`${this.context.styles['app-icon']} ${this.context.styles['orange']}`}>*/}
                                {/*<img src={iconNotes} />*/}
                            {/*</div>*/}
                            {/*<div className={this.context.styles['app-name']}>*/}
                                {/*Notes*/}
                            {/*</div>*/}
                        {/*</div>*/}

                        {/*<div className={this.context.styles['app']} onClick={() => {*/}
                            {/*this.props.history.push('/gallery');*/}
                        {/*}}>*/}
                            {/*<div className={`${this.context.styles['app-icon']} ${this.context.styles['yellow']}`}>*/}
                                {/*<img src={image_gallery} />*/}
                            {/*</div>*/}
                            {/*<div className={this.context.styles['app-name']}>*/}
                                {/*Gallery*/}
                            {/*</div>*/}
                        {/*</div>*/}













                    </div>
                </main>
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        removeClue, bringClueFront, hideDetail
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
