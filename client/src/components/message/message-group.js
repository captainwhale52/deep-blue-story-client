import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import MessageGroupItem from './message-group-item';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        const messageGroups = this.props.messageGroups.map((group, index) => {
            return <MessageGroupItem key={`group-${index}`} history={this.props.history} index={group.pk}
                                     name={group.name} members={group.members} lastMessage={group.messages[group.messages.length - 1]} />
        });
        return (
            <main>
                <div className={this.context.styles['app-name']}>
                    <div></div>
                    <div>Messages</div>
                    <div></div>
                </div>
                <div className={this.context.styles['container']}>
                    {messageGroups}
                </div>
            </main>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
