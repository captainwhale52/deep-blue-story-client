import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import { messageDateTimeFormat } from './../../utils/formatter';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        let classRight = '';
        let username;
        if (this.props.message.username.toLowerCase() === 'karl') {
            classRight = this.context.styles['right'];
            username = <div className={this.context.styles['username']}>
                @Karl
            </div>
        } else {
            username = <div className={this.context.styles['username']}>
                {`@${this.props.message.username}`}
            </div>
        }

        return (
            <div className={`${this.context.styles['message-thread-item']} ${classRight}`}>
                <div>
                    <div className={this.context.styles['value']} dangerouslySetInnerHTML={{__html: this.props.message.value}} />
                    <div className={this.context.styles['username']}>
                        {username}
                    </div>
                </div>
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
