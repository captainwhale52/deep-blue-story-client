import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import MessageGroup from './message-group';
import MessageThread from './message-thread';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        return (
            <div className={this.context.styles['message-app']}>
                <Header />
                <Switch>
                    <Route exact path={`${this.props.match.url}`} component={MessageGroup} />
                    <Route exact path={`${this.props.match.url}/:groupId`} component={MessageThread} />
                </Switch>
                <Footer history={this.props.history} />
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
