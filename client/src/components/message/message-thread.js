import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import MessageThreadItem from './message-thread-item';
import { messageDateTimeFormat } from './../../utils/formatter';

import { markTaskDone } from "./../../events/actions/action-app";
import { sendMessage } from "./../../events/actions/action-message";



class Component extends React.Component {
    constructor(props) {
        super(props);
        this.messageInput = React.createRef();
        this.bottomPadding = React.createRef();
        this.state = {

        };
    }

    componentDidMount() {
        // this.bottomPadding.current.scrollIntoView(false);
        this.props.messageGroups.forEach((group, index) => {
            if (parseInt(this.props.match.params.groupId) === group.pk) {
                this.setState({
                    groupId: parseInt(this.props.match.params.groupId),
                    apiAddress: group.apiAddress,
                    recipient: group.members[0]
                });
            }
        });

        if (parseInt(this.props.match.params.groupId) === 1) {
            this.props.markTaskDone(1);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // this.bottomPadding.current.scrollIntoView(false);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    handleKeyDown = (event) => {
        const currentCode = event.which || event.code;
        let currentKey = event.key;
        if (!currentKey) {
            currentKey = String.fromCharCode(currentCode);
        }

        if (currentKey === 'Enter') {
            this.sendMessage(event.target.value);
            event.target.value = '';
            event.preventDefault();
        }
        // this.bottomPadding.current.scrollIntoView(false);

        // this.setState({
        //     textInputFilled: this.inputText.current.innerHTML
        // });
    };

    sendMessage = (message) => {
        if (message) {
            this.props.sendMessage(parseInt(this.props.match.params.groupId), message, this.state.apiAddress);
        } else if (this.messageInput.current) {
            this.props.sendMessage(parseInt(this.props.match.params.groupId), this.messageInput.current.value, this.state.apiAddress);
            this.messageInput.current.value = '';
        }

    };

    render() {
        const messageGroups = this.props.messageGroups.filter((group, index) => {
            return parseInt(this.props.match.params.groupId) === group.pk;
        });

        if (messageGroups.length === 0) {
            return (<main />);
        }

        const messageThread = messageGroups[0].messages.map((message, index) => {
            return <MessageThreadItem key={`message-${index}`} message={message} />
        });

        return (
            <main>
                <div className={this.context.styles['app-name']}>
                    <div className={this.context.styles['menu']} onClick={() => {
                    this.props.history.goBack();
                }}>{`<`}</div>
                    <div>{messageGroups[0].name}</div>
                    <div />
                </div>
                <div className={`${this.context.styles['container']} ${this.context.styles['message-thread']}`}>
                    {messageThread}
                    <div className={this.context.styles['last-updated']}>
                        {`Last updated: ${messageDateTimeFormat(messageGroups[0].messages[messageGroups[0].messages.length - 1].timestamp)}`}
                    </div>
                    <div className={this.context.styles['bottom-padding']} ref={this.bottomPadding} />
                </div>
                <div className={this.context.styles['text-input']}>
                    <input id="message-input" ref={this.messageInput} disabled={!messageGroups[0].active} type="text" className={this.context.styles['text']} onKeyDown={this.handleKeyDown} placeholder={messageGroups[0].active ? `Click to reply to @${messageGroups[0].members.join(', @')}` : `@${messageGroups[0].members.join(', @')} is offline.`} />
                    <div className={this.context.styles['button']}>
                        <div className={messageGroups[0].active ? this.context.styles['enabled'] : this.context.styles['disabled']} onClick={() => {
                            this.sendMessage();
                        }}>Send</div>
                    </div>
                </div>
            </main>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        messageGroups: state.message.groups,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        sendMessage, markTaskDone
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
