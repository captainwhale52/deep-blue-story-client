import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import moment from 'moment';

import GLOBALS from '../../globals/global-index';

import Header from '../header';
import Footer from '../footer';
import { messageDateTimeFormat } from './../../utils/formatter';


class Component extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {

        };
    }

    render() {
        return (
            <div className={this.context.styles['message-group']} onClick={() => {
                this.props.history.push(`/message/${this.props.index}`);
            }}>
                <div className={this.context.styles['message-label']}>
                    <div>
                        {this.props.name}
                    </div>
                    <div className={this.context.styles['message-timestamp']}>
                        {messageDateTimeFormat(this.props.lastMessage.timestamp)}
                    </div>

                </div>
                <div className={this.context.styles['message-value']}>
                    {`@${this.props.lastMessage.username}: ${this.props.lastMessage.value}`}
                </div>
            </div>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {

    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
