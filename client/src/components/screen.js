import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../globals/global-index';

import { setIsTyping, consumeNewMessage, sendMessage } from "../events/actions/action-app";


class Component extends React.Component {
    constructor(props) {
        super(props);
        this.mainContainer = React.createRef();
        this.inputText = React.createRef();
        this.bottomPadding = React.createRef();
        this.state = {
            textInputFilled: false,
        };
    }

    componentDidMount() {
        document.addEventListener('click', (event) => {
            if (this.inputText.current) {
                this.inputText.current.focus();
            }
        });
        window.addEventListener('keydown', (event) => {
            if (this.inputText.current) {
                this.inputText.current.focus();
            }
        });
    }

    static getDerivedStateFromProps(nextProps, prevState) {

        // this.play = function(buffer, callback) {
        // var myBlob = new Blob([buffer], { type: 'audio/mpeg' });
        // var audio = document.createElement('audio');
        // var objectUrl = window.URL.createObjectURL(myBlob);
        // audio.src = objectUrl;
        // audio.addEventListener('ended', function() {
        // audio.currentTime = 0;
        // if (typeof callback === 'function') {
        // callback();
        // }
        // });
        // audio.play();
        // recorder.clear();
        // };

        return prevState;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        setTimeout(() => {
            if (this.inputText.current) {
                this.inputText.current.focus();
            }
            this.bottomPadding.current.scrollIntoView(false);
        }, 100);
    }

    handleKeyDown = (event) => {
        const currentCode = event.which || event.code;
        let currentKey = event.key;
        if (!currentKey) {
            currentKey = String.fromCharCode(currentCode);
        }

        if (currentKey === 'Enter') {
            this.props.sendMessage('Karl', event.target.innerHTML);
            event.target.innerHTML = '';
            event.preventDefault();
        }
        this.bottomPadding.current.scrollIntoView(false);

        this.setState({
            textInputFilled: this.inputText.current.innerHTML
        });
    };

    render() {
        const messages = this.props.messages.map((message, index) => {
            return <p className={this.context.styles['message-history']} key={`message-${index}`}><span dangerouslySetInnerHTML={{__html: `${message.username}> ${message.value}`}} /></p>
        });

        const messageVisibility = this.props.isWaiting ? this.context.styles['hidden'] : '';
        const textInputFilled = this.state.textInputFilled ? this.context.styles['filled'] : '';

        return (
            <main ref={this.mainContainer}>
                <div>
                    { messages }
                </div>
                <div className={`${this.context.styles['message-current']} ${messageVisibility}`}>
                    <span>{`Karl> `}</span>
                    <span ref={this.inputText} contentEditable={true} className={`${this.context.styles['cursor']} ${textInputFilled}`} onKeyDown={this.handleKeyDown} />
                    <div className={this.context.styles['message-bottom']} ref={this.bottomPadding}>_</div>
                </div>
            </main>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        username: state.app.username,
        messages: state.app.messages,
        isWaiting: state.app.fetching,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        sendMessage
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
