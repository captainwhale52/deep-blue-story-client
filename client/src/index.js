import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
// import { TimelineMax } from "gsap";
import Draggable from 'react-draggable';

import store from './events/stores/store-index';
import styles from './styles/index.scss';

import LockedScreen from './components/locked-screen';
import HomeScreen from './components/home-screen';
import BlankScreen from './components/blank-screen';
import MessageApp from './components/message/message-app';
import CalendarApp from './components/calendar/calendar-app';
import GalleryApp from './components/gallery/gallery-app';
import ScatApp from './components/iframe/schrodingerscat-app';
import TLGApp from './components/iframe/thelastgoodbye-app';
import StayApp from './components/iframe/stay-app';
import GillyApp from './components/iframe/gilly-app';
import Desk from './components/desk/desk-index';
import FoodParentApp from './components/iframe/foodparent-app';
import EscapeGoat2App from './components/iframe/escapegoat2-app';
import MyMusicMateApp from './components/iframe/mymusicmate-app';
import SmartCityApp from './components/iframe/smartcity-app';
import UmlautApp from './components/iframe/u-umlaut-app';
import KarlexaApp from './components/iframe/karlexa-app';


import { isSupportedBrowser, isDesktopBrowser } from './utils/support';


if (window.screen.lockOrientation) {
    window.screen.lockOrientation('portrait');
}

class App extends React.Component {
    getChildContext() {
        return { styles };
    }

    componentDidMount() {
        // console.log(process.env);

        if (!isDesktopBrowser() && window.location.pathname === '/') {
            window.location.replace('/home');
        } else if (isDesktopBrowser() && !store.getState().app.unlocked && window.location.pathname !== '/') {
            window.location.replace('/');
        }

        setTimeout(() => {
            document.body.className = styles['show'];
        }, 750);

        // // Glitch Timeline Function
        // const $filter = document.querySelector('.svg-sprite'),
        //     $turb = $filter.querySelector('#filter feTurbulence'),
        //     turbVal = { val: 0.000001 },
        //     turbValX = { val: 0.000001 };
        //
        // const glitchTimeline = function() {
        //     const timeline = new TimelineMax({
        //         repeat: -1,
        //         repeatDelay: 15,
        //         paused: true,
        //         onUpdate: function () {
        //             $turb.setAttribute('baseFrequency', turbVal.val + ' ' + turbValX.val);
        //         }
        //     });
        //
        //     // timeline
        //     //     .to(turbValX, 0.1, { val: 0.3 })
        //     //     .to(turbVal, 0.1, { val: 0.02 });
        //     // timeline
        //     //     .set(turbValX, { val: 0.000001 })
        //     //     .set(turbVal, { val: 0.000001 });
        //     timeline
        //         .to(turbValX, 0.1, { val: 0.3 })
        //         .to(turbVal, 0.1, { val: 0.02 });
        //     timeline
        //         .to(turbValX, 0.2, { val: 0.4 }, 0.2)
        //         .to(turbVal, 0.2, { val: 0.002 }, 0.2);
        //     timeline
        //         .to(turbValX, 0.2, { val: 0.1 }, 0.1)
        //         .to(turbVal, 0.2, { val: 0.001 }, 0.1);
        //     timeline
        //         .set(turbValX, { val: 0.000001 })
        //         .set(turbVal, { val: 0.000001 });
        //
        //     return {
        //         start: function() {
        //             timeline.play(0);
        //         },
        //         // stop: function() {
        //         //     timeline.pause();
        //         // }
        //     };
        // };
        // new glitchTimeline().start();
    }
    render() {
        if (isSupportedBrowser()) {
            const body = <div>
                <div id="app">
                    <div>
                        <Route exact path='*' component={HomeScreen}/>
                        <Switch>
                            <Route exact path='/' component={LockedScreen} />
                            <Route exact path='/home' component={BlankScreen} />
                            <Route path='/message' component={MessageApp} />
                            <Route path='/calendar' component={CalendarApp} />
                            <Route path='/gallery' component={GalleryApp} />
                            <Route path='/gilly-story' component={GillyApp} />
                            <Route path='/schrodinger-s-cat' component={ScatApp} />
                            <Route path='/the-last-goodbye' component={TLGApp} />
                            <Route path='/stay' component={StayApp} />
                            <Route path='/food-parent' component={FoodParentApp} />
                            <Route path='/escape-goat-2' component={EscapeGoat2App} />
                            <Route path='/my-music-mate' component={MyMusicMateApp} />
                            <Route path='/atlanta-smart-city' component={SmartCityApp} />
                            <Route path='/u-umlaut' component={UmlautApp} />
                            <Route path='/karlexa' component={KarlexaApp} />
                        </Switch>
                        <Link className={styles['power-button']} to='/' />
                    </div>
                </div>
            </div>;
            if (isDesktopBrowser()) {
                return (
                    <Router>
                        <Provider store={store}>
                            <div>
                                <Desk />
                                <Draggable enableUserSelectHack={false}>
                                    {body}
                                </Draggable>
                            </div>
                        </Provider>
                    </Router>
                );
            } else {
                return (
                    <Router>
                        <Provider store={store}>
                            <div>
                                <Desk />
                                {body}
                            </div>
                        </Provider>
                    </Router>
                );
            }
        }
        return (
            <div>
                <div className={styles['warning-background']} />
                <div className={styles['warning']}>Sorry,<br/>Your browser doesn't support major features of this app.<br/><br/>Please use <strong>Chrome</strong> or <strong>Safari</strong> browser.</div>
            </div>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(<App />, document.getElementById("root"));