'use strict';


module.exports.validationBuilder = function(isValid, violatedSlot, messageContent, options) {
    if (messageContent == null) {
        return {
            isValid,
            violatedSlot,
            options
        };
    }
    return {
        isValid,
        violatedSlot,
        message: { contentType: 'PlainText', content: messageContent },
        options
    };
};


module.exports.messageBuilder = function(message, attributes) {
    if (attributes) {
        return {
            contentType: "CustomPayload",
            content: JSON.stringify({
                message: message,
                attributes: attributes
            })
        };
    }
    return {
        contentType: "PlainText",
        content: message
    };
};


module.exports.switchIntent = function(sessionAttributes, intentName, slotToElicit, slots, message) {
    console.log(message);
    return {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message
        }
    };
};


module.exports.close = function(sessionAttributes, fulfillmentState, message) {
  return {
    sessionAttributes,
    dialogAction: {
      type: 'Close',
      fulfillmentState,
      message
    }
  };
};


module.exports.delegate = function(sessionAttributes, slots) {
    return {
        sessionAttributes,
        dialogAction: {
            type: 'Delegate',
            slots,
        },
    };
};


module.exports.elicitSlot = function(sessionAttributes, intentName, slots, slotToElicit, message, title, imageUrl, buttons) {
    if (title === null && buttons === null) {
        return {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message
        }
    };
    }
    return {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
            responseCard: getResponseCard(title, imageUrl, buttons)
        }
    };
};


function getResponseCard(title, imageUrl, buttons) {
    return {
        contentType: 'application/vnd.amazonaws.card.generic',
        genericAttachments: [
            {
                title,
                imageUrl,
                buttons
            }
        ]
    };
}
