'use strict';

const builder = require('../builder');
const profanity = require('profanity-util');

function validate() {
    return builder.validationBuilder(true, null, null, null);
}


module.exports = function(intentRequest) {
    const source = intentRequest.invocationSource;

    // Print out logs
    console.log(`----- INTENT REQUEST -----`);
    console.log(intentRequest);

    if (intentRequest.inputTranscript.toLowerCase().includes('no')) {
        if (!intentRequest.sessionAttributes) {
            intentRequest.sessionAttributes = {};
        }
        return Promise.resolve(builder.close(
            intentRequest.sessionAttributes,
            'Failed',
            builder.messageBuilder(`Who are you then, using Karl's terminal?`)
        ));
    }

    // Handler
    if (source === 'DialogCodeHook') {
        const slots = intentRequest.currentIntent.slots;
        const validationResult = validate();

        if (!validationResult.isValid) {
            slots[`${validationResult.violatedSlot}`] = null;
            if (validationResult.options) {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    validationResult.options.title, validationResult.options.imageUrl, validationResult.options.buttons
                ));
            } else {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    null, null, null
                ));
            }
        }
        return Promise.resolve(builder.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots));

    }

    if (source === 'FulfillmentCodeHook') {
        if (!intentRequest.sessionAttributes) {
            intentRequest.sessionAttributes = {};
        }
        return Promise.resolve(builder.close(
            intentRequest.sessionAttributes,
            'Fulfilled',
            builder.messageBuilder(`Thanks God. I thought you were dead, Karl.`)
        ));
    }
};
