'use strict';

const builder = require('../builder');


function validateUsername(username) {
    if (username && profanity.check(username).length > 0) {
        return builder.validationBuilder(false, 'Username', `I don't think ${username} is your name. Please tell me your real name.`, null);
    }
    return builder.validationBuilder(true, null, null, null);
}


module.exports = function(intentRequest) {
    const source = intentRequest.invocationSource;

    // Print out logs
    console.log(`----- INVOCATION SOURCE -----`);
    console.log(source);
    console.log(`----- SLOTS -----`);
    console.log(intentRequest.currentIntent.slots);
    console.log(`----- SESSION ATTRIBUTE -----`);
    console.log(intentRequest.sessionAttributes);

    // Handler
    if (!intentRequest.sessionAttributes) {
        intentRequest.sessionAttributes = {};
    }
    return Promise.resolve(builder.close(
        intentRequest.sessionAttributes,
        'Fulfilled',
        builder.messageBuilder(`Info about the Centre for Digital Media.`)
    ));
};
