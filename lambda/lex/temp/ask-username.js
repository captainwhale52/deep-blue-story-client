'use strict';

const builder = require('../builder');
const profanity = require('profanity-util');


function formatUsername(username) {
    if (username) {
        const firstName = username.split(" ")[0];
        return firstName.charAt(0).toUpperCase() + firstName.slice(1);
    }
    return null;
}

function validateUsername(username) {
    if (username && profanity.check(username).length > 0) {
        return builder.validationBuilder(false, 'Username', `I don't think ${username} is your name. Please tell me your real name.`, null);
    }
    return builder.validationBuilder(true, null, null, null);
}


module.exports = function(intentRequest) {
    const source = intentRequest.invocationSource;

    // Print out logs
    console.log(`----- INVOCATION SOURCE -----`);
    console.log(source);
    console.log(`----- SLOTS -----`);
    console.log(intentRequest.currentIntent.slots);
    console.log(`----- SESSION ATTRIBUTE -----`);
    console.log(intentRequest.sessionAttributes);

    // Define slots
    const username = formatUsername(intentRequest.currentIntent.slots['Username']);
    // Handler
    if (source === 'DialogCodeHook') {
        const slots = intentRequest.currentIntent.slots;
        const validationResult = validateUsername(username);

        if (!validationResult.isValid) {
            slots[`${validationResult.violatedSlot}`] = null;
            if (validationResult.options) {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    validationResult.options.title, validationResult.options.imageUrl, validationResult.options.buttons
                ));
            } else {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    null, null, null
                ));
            }
        }
        return Promise.resolve(builder.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots));

    } else if (source === 'FulfillmentCodeHook') {
        if (!intentRequest.sessionAttributes) {
            intentRequest.sessionAttributes = {};
        }
        intentRequest.sessionAttributes['Username'] = username;
        return Promise.resolve(builder.close(
            intentRequest.sessionAttributes,
            'Fulfilled',
            builder.messageBuilder(`${username}`)
        ));
    }
};
