'use strict';

// const greetHavenUser = require('./intents/greet-haven-user');
// const selectHavenExperience = require('./intents/select-haven-experience');
// const askHavenFeedback = require('./intents/ask-haven-feedback');

const foodOrder = require('./intents/food-order');
// const askUsername = require('./intents/ask-username');
// const aboutCDM = require('./intents/about-cdm');


module.exports = function(intentRequest) {
    console.log(`----- DISPATCH USERID -----`);
    console.log(intentRequest.userId);
    console.log(`----- INTENT NAME -----`);
    console.log(intentRequest.currentIntent.name);

    const intentName = intentRequest.currentIntent.name;

    if (intentName === 'DBS_Eason_FoodOrder') {
        return foodOrder(intentRequest);
    }

    // if (intentName === 'SelectHavenExperience') {
    //     console.log(`${intentName} is called`);
    //     return selectHavenExperience(intentRequest);
    // }
    //
    // if (intentName === 'AskHavenFeedback') {
    //     console.log(`${intentName} is called`);
    //     return askHavenFeedback(intentRequest);
    // }

    throw new Error(`Intent with a name ${intentName} is not supported`);
};
