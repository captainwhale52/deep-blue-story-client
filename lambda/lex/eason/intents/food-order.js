'use strict';

const builder = require('../../builder');
const profanity = require('profanity-util');


function validateSlots(food, restaurant) {
    if (food && profanity.check(food).length > 0) {
        return builder.validationBuilder(false, 'Food', `I don't think ${food} is what you want to order.`, null);
    }
    if (restaurant && profanity.check(restaurant).length > 0) {
        return builder.validationBuilder(false, 'Food', `I don't you want to order from ${restaurant}.`, null);
    }
    return builder.validationBuilder(true, null, null, null);
}

module.exports = function(intentRequest) {
    const source = intentRequest.invocationSource;

    // Print out logs
    console.log(`----- INVOCATION SOURCE -----`);
    console.log(source);
    console.log(`----- SLOTS -----`);
    console.log(intentRequest.currentIntent.slots);
    console.log(`----- SESSION ATTRIBUTE -----`);
    console.log(intentRequest.sessionAttributes);

    if (!intentRequest.sessionAttributes) {
        intentRequest.sessionAttributes = {};
    }

    // Define slots
    const food = intentRequest.currentIntent.slots['Food'];
    const restaurant = intentRequest.currentIntent.slots['Restaurant'];

    // Handler
    if (source === 'DialogCodeHook') {
        const slots = intentRequest.currentIntent.slots;
        const validationResult = validateSlots(food, restaurant);

        if (!validationResult.isValid) {
            slots[`${validationResult.violatedSlot}`] = null;
            if (validationResult.options) {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    validationResult.options.title, validationResult.options.imageUrl, validationResult.options.buttons
                ));
            } else {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    null, null, null
                ));
            }
        }
        return Promise.resolve(builder.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots));
    }

    if (source === 'FulfillmentCodeHook') {
        return Promise.resolve(builder.close(
            intentRequest.sessionAttributes,
            'Fulfilled',
            builder.messageBuilder(`Sounds great! I will stop by ${restaurant} to get your ${food}. Meet you soon!`)
        ));
    }
};
